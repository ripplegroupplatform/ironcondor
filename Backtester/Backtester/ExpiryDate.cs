using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
  public class ExpiryDate
  {
    public DateTime expiryDate;
    public DateTime currDate;

    public ExpiryDate(DateTime _expiryDate, DateTime _currDate)
    {
      expiryDate = _expiryDate;
      currDate = _currDate;
    }
  }
}
