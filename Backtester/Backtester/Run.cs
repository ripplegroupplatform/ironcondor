using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
    public class Run
    {
        public enum Strategy
        {
            MidStrikeGap = 1,
            EndStrikeGap = 2,
            Adx = 3,
            EMA = 4,
            RSI = 5,
            Aroon = 6

        }

        public enum BarType
        {
            Daily = 1,
            Hourly = 2,
            Minute = 3
        }

        public bool runLoaded = false;

        public long id;
        public string name;
        public string datafile;
        public DateTime startDate;
        public DateTime endDate;
        public BarType barType;
        public decimal startingAccountBalance;
        public decimal endingAccountBalance;
        public string description;
        public DateTime runInit;
        public int testID;
        public dynamic runSettings = new ExpandoObject();
        public decimal availableBalance;
        public decimal commission;

     
        public int maxLossStreak;
        public int maxWinStreak;

    }
}
