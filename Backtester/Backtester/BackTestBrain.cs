﻿using Newtonsoft.Json;
using Skender.Stock.Indicators;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backtester
{
    public  class BackTestBrain 
    {
   
       

        public  DateTime currentRunDate;
        public List<IronCondor> currentHoldings = new List<IronCondor>();
        public Dictionary<long, Run> activeRuns = new Dictionary<long, Run>();
        public List<Settlement> settlements = new List<Settlement>();
     
        public  long runID;

      
        public  Run run = new Run();

        public  List<IronCondor> allTrades = new List<IronCondor>();


        public  bool doAdx = false;
        public  bool doBollinger = false;
        public  bool doMACD = false;
        public  bool doVix = false;
        public  bool doDirectionalRSI = false;
        public  bool doBollingerGap = false;
        public  bool doBollingerBuffer = false;
        public bool doUpperRSI = false;
        public bool doMinBollingerPercent = false;
        public bool cutoffAfterXLosses = false;
        public bool doMidPoint = false;
        public bool doMidPointBuffer = false;
        public bool dontExitWinners = false;


        StringBuilder dbCalls;

        public  int adxLimit = 0;
        public  int adxLength = 0;
        public  int bollingerSD = 0;
        public  int bollingerPeriod = 0;
        public  int macdUpperLimit = 0;
        public  int macdLowerLimit = 0;
        public  int vixLimit = 0;
        public  int directionalRsiLimit = 0;
        public  int directionalAdxLimit = 0;
        public  double bollingerBuffer = 0;
        public int upperRSILimit = 0;
        public double minBollingerPercent = 0;
        public int xLossLimit = 0;
        public decimal midPointBuffer = 0;

        public  bool doDirectionals = false;

        public  double bollingerGapMax = 0;

        public bool doBankRollYearRandomizer = false;


        public  double indicatorSkipDays = 1.0;

        public int daysOutExpiry = 21;

        public StringBuilder updateSqls;

        
        public DataHelper dataHelper;

        public double bankrollBetPercent = 0.1;
        public bool doBankRollBetPercent = false;

        public BackTestBrain(ref StringBuilder updateSqls)
        {
            this.updateSqls = updateSqls;
        }

        public BackTestBrain(Dictionary<long,Run> _activeRuns, DataHelper _dataHelper)
        {
            //this.updateSqls = updateSqls;
            dataHelper = _dataHelper;
            activeRuns = _activeRuns;
         

        }

        public  void RunSingleRun()
        {
            RunSettings();
            //addRunToDB();
            object obj = new object();
            Run(obj);
        }


        // FILL THESE OUT FOR A RUN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        public  void RunSettings()
        {
            run.runInit = DateTime.Now;
            run.startDate = Convert.ToDateTime("2016-01-01");
            run.endDate = Convert.ToDateTime("2021-09-30");
            run.startingAccountBalance = 500000;
            run.endingAccountBalance = 500000;
            run.barType = Backtester.Run.BarType.Daily;
            run.description = "ADX 25, macd, vix 25, directionals";

            run.runSettings.midGap = 0.10;
            run.runSettings.endGap = 20;
            run.runSettings.adxLimit = 25;
            run.runSettings.adxLength = 14;
            run.runSettings.bollingerPeriod = 30;
            run.runSettings.bollingerSD = 2;
            run.runSettings.macdUpperLimit = 25;
            run.runSettings.macdLowerLimit = -25;
            run.runSettings.vixLimit = 25;
            run.runSettings.indicatorSkipDays = 30;
            // run.runSettings.bollingerGapMax = 0.06;
            // run.runSettings.bollingerBuffer = 0.01;

            run.runSettings.doDirectionals = true;
            run.runSettings.directionalRsiLimit = 45;
            run.runSettings.directionalAdxLimit = 30;



            //run.datafile = @"C:\Projects\IronCondor\IronCondor\2017\combined.csv";
        }
        //END


        public  void SetupIndicators()
        {

            try
            {
                adxLimit = run.runSettings.adxLimit;
                adxLength = run.runSettings.adxLength;

                if (Convert.ToDouble(adxLength) > indicatorSkipDays)
                    indicatorSkipDays = Convert.ToDouble(adxLength);

                doAdx = true;

            }
            catch (Exception)
            {

            }

            try
            {
                bankrollBetPercent = run.runSettings.bankrollBetPercent;
                doBankRollBetPercent = true;
            }
            catch(Exception)
            {

            }

            try
            {
                daysOutExpiry = run.runSettings.daysOutExpiry;

                if (Convert.ToDouble(daysOutExpiry) > indicatorSkipDays)
                    indicatorSkipDays = Convert.ToDouble(daysOutExpiry);

            }
            catch (Exception)
            {

            }


            try
            {
                bollingerSD = run.runSettings.bollingerSD;
                bollingerPeriod = run.runSettings.bollingerPeriod;

                if (Convert.ToDouble(bollingerPeriod) > indicatorSkipDays)
                    indicatorSkipDays = Convert.ToDouble(bollingerPeriod);

                doBollinger = true;

            }
            catch (Exception)
            {

            }


            //add a buffer to the bollinger bands
            try
            {

                bollingerBuffer = run.runSettings.bollingerBuffer;
                doBollingerBuffer = true;

            }
            catch (Exception)
            {

            }

            try
            {
                macdLowerLimit = run.runSettings.macdLowerLimit;
                macdUpperLimit = run.runSettings.macdUpperLimit;
                doMACD = true;

            }
            catch (Exception)
            {

            }



            try
            {
                macdLowerLimit = run.runSettings.macdLowerLimit;
                macdUpperLimit = run.runSettings.macdUpperLimit;
                doMACD = true;

            }
            catch (Exception)
            {

            }


            try
            {
                dontExitWinners = Convert.ToBoolean(run.runSettings.dontExitWinners);

            }
            catch (Exception)
            {

            }



            try
            {
                xLossLimit = run.runSettings.xLossLimit;
                cutoffAfterXLosses = true;

            }
            catch (Exception)
            {

            }

            try
            {
                midPointBuffer = run.runSettings.midPointBuffer;
                doMidPointBuffer = true;
            }
            catch (Exception)
            {

            }

            try
            {
                doMidPoint = Convert.ToBoolean(run.runSettings.doMidPoint);
            }
            catch (Exception)
            {

            }


            try
            {
                vixLimit = run.runSettings.vixLimit;
                doVix = true;

            }
            catch (Exception)
            {

            }

            try
            {
                directionalRsiLimit = run.runSettings.directionalRsiLimit;
                doDirectionalRSI = true;
            }
            catch (Exception)
            {

            }


            //avoid betting in super thin market
            try
            {
                minBollingerPercent = run.runSettings.minBollingerPercent;
                doMinBollingerPercent = true;
            }
            catch (Exception)
            {

            }
            

            try
            {
                directionalAdxLimit = run.runSettings.directionalAdxLimit;
            }
            catch (Exception)
            {

            }


            //try to prevent betting in balloon
            try
            {
                bollingerGapMax = run.runSettings.bollingerGapMax;
                doBollingerGap = true;
            }
            catch (Exception)
            {

            }

            try
            {
                upperRSILimit = run.runSettings.upperRSILimit;
                doUpperRSI = true;
            }
            catch (Exception)
            {

            }


            try
            {
                doDirectionals = Convert.ToBoolean(run.runSettings.doDirectionals);
            }
            catch (Exception e)
            {

            }

            try
            {
                doBankRollYearRandomizer = Convert.ToBoolean(run.runSettings.doBankRollYearRandomizer);
            }
            catch(Exception e)
            {

            }

        }

        public  void Run(object obj)
        {
            //dataHelper.removeFiles(@"C:\Data\RUTW\opt_taq_1min\2018\");
            //dataHelper.setupData(@"F:\Data\2016\", true);
            //dataHelper.combineData("2016");
            //dataHelper.fillInData(run.datafile);

            dbCalls = new StringBuilder("", 200 * 200);

            run.availableBalance = run.startingAccountBalance;

            //Console.WriteLine("STARTING RUN " + run.id);

            // activeRuns = new List<Run>((IEnumerable<Run>)obj);

            allTrades = new List<IronCondor>();
            run.runInit = DateTime.Now;

            //updateRun();

            SetupIndicators();

            dataHelper.setupDBData(run);

            currentRunDate = run.startDate;

            for (var x = 0; x < dataHelper.quoteDates.Count; x++)
            {
                currentRunDate = dataHelper.quoteDates[x];
                {
                    DateTime s = DateTime.Now;
                    scoreRun(currentRunDate);
                    handleData(currentRunDate);
                }

                currentRunDate = currentRunDate.AddDays(1);
            }

            endRun();
        }


        public  void handleData(DateTime runDate)
        {

            //cant look at first x days due to sma(x) 
            if ((runDate - run.startDate).Days < indicatorSkipDays)
                return;

            /*
            ISupportResistanceCalculator resistanceCalculator = new SupportResistanceCalculator();
            List<float> indexDecimals = dataHelper.myIndexQuotes.Select(x => float.Parse(x.Close.ToString())).ToList();

            var priorTradeDay = dataHelper.quoteDates.Where(x => x.Date.Date <= runDate.Date.AddDays(-30)).FirstOrDefault();

            var startSupResIndex = dataHelper.myIndexQuotes.ToList().FindIndex(x => x.Date.Date == priorTradeDay.Date.Date);
            var endSupResIndex = dataHelper.myIndexQuotes.ToList().FindIndex(x => x.Date.Date == runDate.Date);
            float supResThreshold = float.Parse("0.015");

            var supRes = resistanceCalculator.GetSupportResistance(indexDecimals, startSupResIndex, endSupResIndex, (endSupResIndex - startSupResIndex), supResThreshold);
            */

            IndexQuote lastClose = dataHelper.GetOrAddRutLastClose(runDate);


            var expDateObj = dataHelper.GetDataForExpiryDict(runDate, daysOutExpiry);



            if(cutoffAfterXLosses)
            {
               
                if(currentHoldings.Count > 0 && settlements.Count > 0) //&& settlements.Reverse().ToList().Take(1)
                {
                    var lastSettle = settlements.LastOrDefault();

                    if (lastSettle.profitLoss < 0 && lastSettle.settlementType != Settlement.SettlementType.Exited)
                    {
                        //only exit from losing ones along with this?
                        List<IronCondor> myHoldings = currentHoldings.Where(x => x.tradeType == IronCondor.TradeType.IronCondor).ToList();

                        foreach (var holding in myHoldings)
                        {
                            exitCondor(holding, runDate);
                        }
                    }

                }

            }



            if (expDateObj != null && expDateObj.Expiry.Date <= run.endDate.Date)
            {
                var expDate = expDateObj.Expiry;

                if (!haveThreeWeekOutOption(currentRunDate, expDate))
                {


                    AdxResult currentAdx = new AdxResult();
                    BollingerBandsResult currentBollinger = new BollingerBandsResult();
                    MacdResult currentMACD = new MacdResult();
                    IndexQuote vixLastClose = new IndexQuote();
                    RsiResult currentRSI = new RsiResult();

                    double currBollingerGap = 0;

                    double maxRisk = 0;


                    if (doAdx)
                    {
                        currentAdx = dataHelper.GetAdxOnDate(lastClose.Date, adxLength);
                    }

                    if (doBollinger)
                    {
                        currentBollinger = dataHelper.GetBollingerOnDate(lastClose.Date, bollingerPeriod, bollingerSD);
                    }

                    if (doBollingerGap || doMinBollingerPercent)
                    {
                        currBollingerGap = (double)((currentBollinger.UpperBand - currentBollinger.LowerBand) / lastClose.Close);
                    }



                    if (doMACD)
                    {
                        currentMACD = dataHelper.GetMACDOnDate(lastClose.Date);
                    }

                    if (doVix)
                    {
                        vixLastClose = dataHelper.GetOrAddVixLastClose(runDate);
                    }

                    if (doUpperRSI)
                    {
                        currentRSI = dataHelper.GetRSIOnDate(runDate, 14); 
                    }

                   

                    if (indicatorsPass(currentAdx, currentBollinger, currentMACD, vixLastClose, currBollingerGap, currentRSI))
                    {
                        int midGap = Convert.ToInt32(lastClose.Close * Convert.ToDecimal(run.runSettings.midGap));
                        int endGap = run.runSettings.endGap;

                        int cStrike = Convert.ToInt32(lastClose.Close + midGap / 2);
                        int dStrike = Convert.ToInt32(lastClose.Close + midGap / 2 + endGap);

                        int bStrike = Convert.ToInt32(lastClose.Close - midGap / 2);
                        int aStrike = Convert.ToInt32(lastClose.Close - midGap / 2 - endGap);

                        


                        if (doBollinger)
                        {

                            //if topprice = currprice / currprice > 20 , check vix % and switch to a percent gap based on vix, tiered in a dicts

                            cStrike = Convert.ToInt32(currentBollinger.UpperBand.Value);

                            if (doBollingerBuffer)
                                cStrike = Convert.ToInt32(cStrike + (cStrike * bollingerBuffer));

                            /*
                            if (doMinBollingerPercent && currBollingerGap < minBollingerPercent)
                                cStrike = Convert.ToInt32(cStrike + (cStrike * .02));
                            */

                            dStrike = cStrike + endGap;

                            bStrike = Convert.ToInt32(currentBollinger.LowerBand.Value);

                            if (doBollingerBuffer)
                                bStrike = Convert.ToInt32(bStrike - (bStrike * bollingerBuffer));

                            /*
                            if (doMinBollingerPercent && currBollingerGap < minBollingerPercent)
                                bStrike = Convert.ToInt32(bStrike - (bStrike *.02));
                            */

                            aStrike = bStrike - endGap;
                        }

                        DateTime runDate1000 = runDate.AddHours(10);
                        //DateTime runDate1100 = runDate.AddHours(11);
                        //DateTime runDate1300 = runDate.AddHours(13);


                        try
                        {
                            //Console.WriteLine("DOING IRON CONDOR:" + run.id);
                           // DateTime s = DateTime.Now;
                            List<OptionQuote> myDayQuotes = dataHelper.GetDataForDayDict(runDate1000);

                            //List<OptionQuote> myDayQuotes = dataHelper.myQuotes.Where(x => x.Date.Date >= runDate && x.Date.Date <=runDate.AddDays(1).Date).ToList();

                            //Console.WriteLine("IRON CONDOR " + +run.id + " TOOK: " + (DateTime.Now - s).TotalMilliseconds);

                            //form iron condor
                            //var quoteC = myDayQuotes.OrderBy(x => x.Strike).FirstOrDefault(x =>
                            //x.Strike >= (cStrike) && x.CallPut == CallPut.CALL && x.Expiry == expDate &&
                            //(x.Date == runDate1000));
                            //var quoteD = myDayQuotes.OrderBy(x => x.Strike).FirstOrDefault(x =>
                            //x.Strike >= (dStrike) && x.Strike != quoteC.Strike && x.CallPut == CallPut.CALL &&
                            //x.Expiry == expDate &&
                            //(x.Date == runDate1000));

                            //var quoteB = myDayQuotes.OrderByDescending(x => x.Strike).FirstOrDefault(x =>
                            //x.Strike <= (bStrike) && x.CallPut == CallPut.PUT && x.Expiry == expDate &&
                            //(x.Date == runDate1000));
                            //var quoteA = myDayQuotes.OrderByDescending(x => x.Strike).FirstOrDefault(x =>
                            //x.Strike <= (aStrike) && x.Strike != quoteB.Strike && x.CallPut == CallPut.PUT && x.Expiry == expDate &&
                            //(x.Date == runDate1000));

                            var quoteC = dataHelper.GetQuoteDayExpiryDateDict(cStrike, -5, CallPut.CALL, expDate, runDate1000);
                            var quoteD = dataHelper.GetQuoteDayExpiryDateDict(dStrike, quoteC.Strike, CallPut.CALL,expDate , runDate1000);

                            var quoteB = dataHelper.GetQuoteDayExpiryDateDict(bStrike, -5, CallPut.PUT, expDate , runDate1000);
                            var quoteA = dataHelper.GetQuoteDayExpiryDateDict(aStrike, quoteB.Strike, CallPut.PUT, expDate, runDate1000);


                            var tradeC = new Trade(getCloseBid(quoteC), 1, quoteC.Strike, quoteC.Expiry, quoteC.CallPut, BuySell.SELL,
                            quoteC.Date, quoteC.CloseBidVolume + quoteC.CloseAskVolume);
                            var tradeD = new Trade(getCloseAsk(quoteD), 1, quoteD.Strike, quoteD.Expiry, quoteD.CallPut, BuySell.BUY,
                            quoteD.Date, quoteD.CloseBidVolume + quoteD.CloseAskVolume);

                            var tradeB = new Trade(getCloseBid(quoteB), 1, quoteB.Strike, quoteB.Expiry, quoteB.CallPut, BuySell.SELL,
                            quoteB.Date, quoteB.CloseBidVolume + quoteD.CloseAskVolume);
                            var tradeA = new Trade(getCloseAsk(quoteA), 1, quoteA.Strike, quoteA.Expiry, quoteA.CallPut, BuySell.BUY,
                            quoteA.Date, quoteA.CloseBidVolume + quoteD.CloseAskVolume);


                           

                            var ironCondor = new IronCondor();
                            ironCondor.commission = 20;
                            ironCondor.tradeDate = quoteC.Date;
                            ironCondor.underlyingDate = lastClose.Date;
                            ironCondor.underlyingPrice = lastClose.Close;
                            ironCondor.underlyingPriceAtStart = lastClose.Close;
                            ironCondor.parts.Add(tradeA);
                            ironCondor.parts.Add(tradeB);
                            ironCondor.parts.Add(tradeC);
                            ironCondor.parts.Add(tradeD);
                            addIndicatorJSON(ironCondor, currentAdx, currentMACD, currentBollinger, vixLastClose, currBollingerGap);

                            if (doBankRollBetPercent)
                            {
                                maxRisk = (tradeD.strike - tradeC.strike) * 100;

                                if ((tradeB.strike - tradeA.strike) * 100 > maxRisk)
                                    maxRisk = (tradeB.strike - tradeA.strike) * 100;

                                int contracts = GetSetBetContracts(ironCondor, maxRisk, run);

                                ironCondor.commission += (1 * contracts * 4);


                                if (contracts > 0)
                                    placeOrder(ironCondor);
                                else
                                    Console.WriteLine("NO MONEY FOR 1 CONTRACT");

                            }
                            else
                                placeOrder(ironCondor);


                        }
                        catch (Exception noSomethingOrderRelatedFound)
                        {
                            string breakPt = "";
                        }
                    }
                    else if (doDirectionals) //indicators didnt pass lets try directional trade
                    {
                       // DateTime dT =  DateTime.Now;

                        RsiResult rsiResult = dataHelper.GetRSIOnDate(lastClose.Date,14);

                        if (doDirectionalRSI && rsiResult.Rsi > directionalRsiLimit && ((Math.Abs(currentMACD.Signal.Value) + Math.Abs(currentMACD.Macd.Value) > 10) || currentAdx.Adx > directionalAdxLimit))
                        {
                            DateTime runDate1000 = runDate.AddHours(10);

                            List<OptionQuote> myDayQuotes = dataHelper.GetDataForDayDict(runDate1000); 

                            //List<OptionQuote> myDayQuotes = dataHelper.myQuotes.Where(x => x.Date.Date >= runDate && x.Date.Date <= runDate.AddDays(1).Date).ToList();

                            BollingerBandsResult slimmerBB = dataHelper.GetBollingerOnDate(lastClose.Date, 30, 1);

                            int endGap = run.runSettings.endGap; //50 for 1 sided?

                            int cStrike = Convert.ToInt32(slimmerBB.Sma);
                            int dStrike = cStrike + endGap;

                            int bStrike = Convert.ToInt32(slimmerBB.Sma);
                            int aStrike = bStrike - endGap;

                     

                            IronCondor.TradeType tradeType = IronCondor.TradeType.IronCondor;

                            var ironCondor = new IronCondor();
                            ironCondor.commission = 20;

                            ironCondor.underlyingDate = lastClose.Date;
                            ironCondor.underlyingPrice = lastClose.Close;
                            ironCondor.underlyingPriceAtStart = lastClose.Close;

                            if (currentAdx.Mdi > currentAdx.Pdi && Math.Abs(currentAdx.Mdi.Value) - currentAdx.Pdi.Value > 10 && currentMACD.Macd < currentMACD.Signal)   //bearish
                            {
                                tradeType = IronCondor.TradeType.ShortCall_VerticalSpread;

                                var quoteC = dataHelper.GetQuoteDayExpiryDateDict(cStrike, -5, CallPut.CALL, expDate, runDate1000);
                                var quoteD = dataHelper.GetQuoteDayExpiryDateDict(dStrike, quoteC.Strike , CallPut.CALL , expDate , runDate1000);


                                var tradeC = new Trade(getCloseBid(quoteC), 1, quoteC.Strike, quoteC.Expiry, quoteC.CallPut, BuySell.SELL,
                                quoteC.Date, quoteC.CloseBidVolume + quoteC.CloseAskVolume);
                                var tradeD = new Trade(getCloseAsk(quoteD), 1, quoteD.Strike, quoteD.Expiry, quoteD.CallPut, BuySell.BUY,
                                quoteD.Date, quoteD.CloseBidVolume + quoteD.CloseAskVolume);


                                ironCondor.parts.Add(tradeD);
                                ironCondor.parts.Add(tradeC);


                                ironCondor.tradeDate = quoteC.Date;


                                ironCondor.tradeType = tradeType;

                                addIndicatorJSON(ironCondor, currentAdx, currentMACD, currentBollinger, vixLastClose, currBollingerGap, rsiResult);

                                

                                if(doBankRollBetPercent)
                                {
                                    maxRisk = (tradeD.strike - tradeC.strike) * 100;
                                    int contracts = GetSetBetContracts(ironCondor, maxRisk, run);

                                    ironCondor.commission += (1 * contracts * 2);

                                    if (contracts > 0)
                                        placeOrder(ironCondor);
                                    else
                                        Console.WriteLine("NO MONEY FOR 1 CONTRACT");
                                }
                                else
                                    placeOrder(ironCondor);

                            }
                            else if (currentAdx.Pdi > currentAdx.Mdi && Math.Abs(currentAdx.Pdi.Value) - currentAdx.Mdi.Value > 10 && currentMACD.Macd > currentMACD.Signal) //bullish
                            {

                                tradeType = IronCondor.TradeType.ShortPut_VerticalSpread;

                                var quoteB = dataHelper.GetQuoteDayExpiryDateDict(bStrike, -5, CallPut.PUT,expDate ,runDate1000);
                                var quoteA = dataHelper.GetQuoteDayExpiryDateDict( aStrike,  quoteB.Strike , CallPut.PUT, expDate , runDate1000);


                                var tradeB = new Trade(getCloseBid(quoteB), 1, quoteB.Strike, quoteB.Expiry, quoteB.CallPut, BuySell.SELL,
                                quoteB.Date, quoteB.CloseBidVolume + quoteB.CloseAskVolume);
                                var tradeA = new Trade(getCloseAsk(quoteA), 1, quoteA.Strike, quoteA.Expiry, quoteA.CallPut, BuySell.BUY,
                                quoteA.Date, quoteA.CloseBidVolume + quoteA.CloseAskVolume);

                                ironCondor.tradeDate = quoteB.Date;


                                ironCondor.parts.Add(tradeA);
                                ironCondor.parts.Add(tradeB);

                                ironCondor.tradeType = tradeType;

                                addIndicatorJSON(ironCondor, currentAdx, currentMACD, currentBollinger, vixLastClose, currBollingerGap, rsiResult);


                                if (doBankRollBetPercent)
                                {
                                    maxRisk = (tradeB.strike - tradeA.strike) * 100;
                                    int contracts = GetSetBetContracts(ironCondor, maxRisk, run);

                                    ironCondor.commission += (1 * contracts * 2);

                                    if (contracts > 0)
                                        placeOrder(ironCondor);
                                    else
                                        Console.WriteLine("NO MONEY FOR 1 CONTRACT");
                                }
                                else
                                    placeOrder(ironCondor);

                            }

                          //  Console.WriteLine("Directional: " + run.id + " took: " + ((DateTime.Now) - dT).TotalMilliseconds);
                        }
                    }
                }
            }

        }
        

        public int GetSetBetContracts(IronCondor ironCondor, double maxRisk, Run incRun)
        {
            int contracts = 1;

            if (doBankRollBetPercent)
            {
                double maxBet = Convert.ToDouble(incRun.availableBalance) * bankrollBetPercent;

                contracts = Convert.ToInt32(maxBet) / Convert.ToInt32(maxRisk);
                ironCondor.contracts = contracts;
                ironCondor.maxRisk = maxRisk;
            }

            return contracts;
        }

        public int GetBetContracts( double maxRisk, Run incRun)
        {
            int contracts = 1;

            if (doBankRollBetPercent)
            {
                double maxBet = Convert.ToDouble(incRun.availableBalance) * bankrollBetPercent;

                contracts = Convert.ToInt32(maxBet) / Convert.ToInt32(maxRisk);
            }

            return contracts;
        }



        public  void addIndicatorJSON(IronCondor ic, AdxResult _currentAdx, MacdResult _currentMACD, BollingerBandsResult _currentBollinger, IndexQuote _vixLastClose, double currentBBGap, RsiResult _rsiResult = null)
        {
            dynamic d = new ExpandoObject();

            d.adx = _currentAdx.Adx.Value;
            d.adxMdi = _currentAdx.Mdi.Value;
            d.adxPdi = _currentAdx.Pdi.Value;

            if(_currentMACD != null)
            { 
                d.macdSignal = _currentMACD.Signal.Value;
                d.macdValue = _currentMACD.Macd.Value;
            }

            d.bollingerUpper = _currentBollinger.UpperBand.Value;
            d.bollingerLower = _currentBollinger.LowerBand.Value;
            d.bollingerSMA = _currentBollinger.Sma.Value;
            d.currentBBGap = currentBBGap;
            d.vix = _vixLastClose.Close;

            if (_rsiResult != null)
                d.rsiResult = _rsiResult.Rsi;

            ic.indicatorJSON = d;
        }


        public  bool indicatorsPass(AdxResult currentAdx, BollingerBandsResult currentBollinger, MacdResult currentMACD, IndexQuote vixLastClose, double currBollingerGap, RsiResult currentRSI)
        {
            bool retVal =  (!doAdx || (doAdx && currentAdx.Adx <= adxLimit)) && (!doMACD || (doMACD && currentMACD.Signal.Value >= macdLowerLimit && currentMACD.Signal.Value <= macdUpperLimit)) && (!doVix || (doVix && vixLastClose.Close <= vixLimit) && (!doBollingerGap || currBollingerGap < bollingerGapMax) && (!doUpperRSI || currentRSI.Rsi < upperRSILimit) && (!doMinBollingerPercent || (currBollingerGap > minBollingerPercent)));


            //&& currentMACD.Macd.Value >=macdLowerLimit && currentMACD.Macd.Value <= macdUpperLimit


            return retVal;
        }

        public  bool haveThreeWeekOutOption(DateTime date, DateTime closestExp)
        {
            bool retVal = false;

            foreach (var holding in currentHoldings)
            {
                if (holding.parts[0].expiry == closestExp)
                    retVal = true;

            }

            if (allTrades.Count > 0 && allTrades.Exists(x => x.tradeDate.Date > date.Date.AddDays(-6)))
                retVal = true;

            return retVal;
        }


        public  void placeOrder(IronCondor ic)
        {
            currentHoldings.Add(ic);
            run.endingAccountBalance -= ic.commission;

            run.commission += ic.commission;
            run.availableBalance = run.endingAccountBalance - Convert.ToDecimal(ic.maxRisk);

            addIronCondorToDB(ic);

            allTrades.Add(ic);

            //check if volume is available on each part
        }

        public decimal getCloseBid(OptionQuote q)
        {
            decimal retVal = 0;

            if (q.CloseBid == 0)
                retVal = q.CloseAsk;
            else
            {
                if (!doMidPoint)
                    retVal = q.CloseBid;
                else
                {
                    retVal = q.Close;

                    if (doMidPointBuffer)
                        retVal = q.Close - (q.Close * midPointBuffer);

                }
            }


            return retVal;
        }

        public decimal getCloseAsk(OptionQuote q)
        {
            decimal retVal = 0;

            if (q.CloseAsk == 0)
                retVal = q.CloseBid;
            else
            {

                if (!doMidPoint)
                    retVal = q.CloseAsk;
                else
                {
                    retVal = q.Close;

                    if (doMidPointBuffer)
                        retVal = q.Close - (q.Close * midPointBuffer);
                }
            }


            return retVal;
        }


        public void exitCondor(IronCondor ironCondor, DateTime currDate)
        {
        
            IndexQuote lastClose = dataHelper.GetOrAddRutLastClose(currDate);

            if(!dontExitWinners || (dontExitWinners && lastClose.Close > ironCondor.parts[2].strike || lastClose.Close < ironCondor.parts[1].strike)) //only close out current losers
            {


                try
                {
                    //find curr prices of all the legs at 10am and 
                    DateTime runDate1000 = currDate.AddHours(10);

                    var quoteC = dataHelper.GetQuoteDayExpiryDateDict(ironCondor.parts[2].strike, -5, CallPut.CALL, ironCondor.parts[2].expiry, runDate1000);
                    var quoteD = dataHelper.GetQuoteDayExpiryDateDict(ironCondor.parts[3].strike, quoteC.Strike, CallPut.CALL, ironCondor.parts[3].expiry, runDate1000);

                    var quoteB = dataHelper.GetQuoteDayExpiryDateDict(ironCondor.parts[1].strike, -5, CallPut.PUT, ironCondor.parts[1].expiry, runDate1000);
                    var quoteA = dataHelper.GetQuoteDayExpiryDateDict(ironCondor.parts[0].strike, quoteB.Strike, CallPut.PUT, ironCondor.parts[0].expiry, runDate1000);

                    decimal netCredit = (ironCondor.parts[1].price * ironCondor.parts[1].amount) + (ironCondor.parts[2].price * ironCondor.parts[2].amount) - (ironCondor.parts[0].price * ironCondor.parts[0].amount) - (ironCondor.parts[3].price * ironCondor.parts[3].amount);


                    netCredit = netCredit + getCloseBid(quoteD) + getCloseBid(quoteA) - getCloseAsk(quoteC) - getCloseAsk(quoteB);

                    Settlement s = new Settlement();
                    s.dateCreated = currDate;
                    s.ironCondorID = ironCondor.id;
                    s.settlementType = Settlement.SettlementType.Exited;

                    s.contracts = ironCondor.contracts;

                    s.profitLoss = netCredit * 100 * ironCondor.contracts;

                    s.commission = 20 + (1 * 4 * ironCondor.contracts);

                    run.endingAccountBalance -= s.commission;

                    run.endingAccountBalance += s.profitLoss;

                    run.availableBalance += s.profitLoss + Convert.ToDecimal(ironCondor.maxRisk);

                    s.underlyingDate = lastClose.Date;
                    s.underlyingPrice = lastClose.Close;
                    s.ironCondor = ironCondor;

                    ironCondor.settlement = s;

                    addSettlementToDB(s);

                    currentHoldings.RemoveAll(x => x.id == ironCondor.id);
                }
                catch(Exception wtf)
                {
                    Console.WriteLine("ironCondor.parts[3].strike " + ironCondor.parts[3].strike + "  ...   " + wtf.ToString());
                }
            }

        }

        public  void scoreRun(DateTime currDate)
        {
            List<long> condorsToRemove = new List<long>();
          
            foreach(var ic in currentHoldings)
            {
                if (currDate > ic.parts.FirstOrDefault().expiry)
                {
                    settleOption(currDate, ic);
                    condorsToRemove.Add(ic.id);
                }
            }
          
            foreach(var id in condorsToRemove)
                currentHoldings.RemoveAll(x=>x.id == id);

        }



        public void settleOption(DateTime currDate, IronCondor ic)
        {
            Settlement s = new Settlement();
            s.dateCreated = currDate;
            s.ironCondorID = ic.id;

            IndexQuote lastClose = dataHelper.GetOrAddRutLastClose(currDate);

            decimal netCredit = 0;

            if (ic.tradeType == IronCondor.TradeType.IronCondor)
            {
                netCredit = (ic.parts[1].price * ic.parts[1].amount) + (ic.parts[2].price * ic.parts[2].amount) - (ic.parts[0].price * ic.parts[0].amount) - (ic.parts[3].price * ic.parts[3].amount);

                if (lastClose.Close > ic.parts[1].strike && lastClose.Close < ic.parts[2].strike)
                {
                    s.settlementType = Settlement.SettlementType.ExpiredOTM;
                    s.profitLoss = netCredit;
                }
                else
                {
                    s.settlementType = Settlement.SettlementType.ExpiredITM;

                    if (lastClose.Close > ic.parts[3].strike)
                        s.profitLoss = netCredit - (ic.parts[3].strike * ic.parts[3].amount - ic.parts[2].strike * ic.parts[2].amount);
                    else if (lastClose.Close > ic.parts[2].strike)
                        s.profitLoss = netCredit - (lastClose.Close * ic.parts[3].amount - ic.parts[2].strike * ic.parts[2].amount);
                    else if (lastClose.Close < ic.parts[0].strike)
                        s.profitLoss = netCredit - (ic.parts[1].strike * ic.parts[1].amount - ic.parts[0].strike * ic.parts[0].amount);
                    else if (lastClose.Close < ic.parts[1].strike)
                        s.profitLoss = netCredit - (lastClose.Close * ic.parts[1].amount - ic.parts[0].strike * ic.parts[0].amount);

                }

            }
            else if (ic.tradeType == IronCondor.TradeType.ShortCall_VerticalSpread)
            {
                netCredit = (ic.parts[1].price * ic.parts[1].amount) - (ic.parts[0].price * ic.parts[0].amount);

                if (lastClose.Close < ic.parts[1].strike)
                {
                    s.settlementType = Settlement.SettlementType.ExpiredOTM;
                    s.profitLoss = netCredit;
                }

                else
                {
                    s.settlementType = Settlement.SettlementType.ExpiredITM;

                    if (lastClose.Close > ic.parts[0].strike)
                        s.profitLoss = netCredit - (ic.parts[0].strike * ic.parts[0].amount - ic.parts[1].strike * ic.parts[1].amount);
                    else if (lastClose.Close > ic.parts[1].strike)
                        s.profitLoss = netCredit - (lastClose.Close * ic.parts[0].amount - ic.parts[1].strike * ic.parts[1].amount);

                }

            }
            else if (ic.tradeType == IronCondor.TradeType.ShortPut_VerticalSpread)
            {
                netCredit = (ic.parts[1].price * ic.parts[1].amount) - (ic.parts[0].price * ic.parts[0].amount);

                if (lastClose.Close > ic.parts[1].strike)
                {
                    s.settlementType = Settlement.SettlementType.ExpiredOTM;
                    s.profitLoss = netCredit;
                }

                else
                {
                    s.settlementType = Settlement.SettlementType.ExpiredITM;

                    if (lastClose.Close < ic.parts[0].strike)
                        s.profitLoss = netCredit - (ic.parts[1].strike * ic.parts[1].amount - ic.parts[0].strike * ic.parts[0].amount);
                    else if (lastClose.Close < ic.parts[1].strike)
                        s.profitLoss = netCredit - (ic.parts[1].amount * ic.parts[1].strike - ic.parts[1].amount * lastClose.Close);

                }

            }
            s.contracts = ic.contracts;

            s.profitLoss = s.profitLoss * 100 * ic.contracts;

            s.commission = ic.commission;

            run.endingAccountBalance += s.profitLoss;

            run.availableBalance += s.profitLoss + Convert.ToDecimal(ic.maxRisk);

            s.underlyingDate = lastClose.Date;
            s.underlyingPrice = lastClose.Close;
            s.ironCondor = ic;

            ic.settlement = s;

            addSettlementToDB(s);

        }


        public  void addIronCondorToDB(IronCondor ic)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(ic.indicatorJSON);

            using (LocalDatabase ldb = new LocalDatabase())
            {
                var sql = "";

                if (ic.tradeType == IronCondor.TradeType.IronCondor)
                {
                    sql = @"insert into ironcondors (runID, commission, dateCreated, strikeA,strikeB,strikeC,strikeD, expiryA,expiryB,expiryC,expiryD, priceA, priceB, priceC, priceD, callPutA, callPutB,callPutC, callPutD, underlyingDate,underlyingPrice, indicatorJSON, volA, volB, volC, volD , contracts)";
                    sql += $" VALUES ({run.id}, {ic.commission}, '{ ic.tradeDate}', { ic.parts[0].strike}, { ic.parts[1].strike}, { ic.parts[2].strike}, {ic.parts[3].strike },  ";
                    sql += $"'{ic.parts[0].expiry }', '{ ic.parts[1].expiry}', '{ic.parts[2].expiry}', '{ ic.parts[3].expiry}', {ic.parts[0].price}, {ic.parts[1].price}, {ic.parts[2].price}, {ic.parts[3].price},  ";
                    sql += $"{ Convert.ToInt32(ic.parts[0].callPut) }, { Convert.ToInt32(ic.parts[1].callPut)}, {Convert.ToInt32(ic.parts[2].callPut)}, {Convert.ToInt32(ic.parts[3].callPut)}, '{ic.underlyingDate}', {ic.underlyingPrice}, '{json}', {ic.parts[0].volume}, {ic.parts[1].volume}, {ic.parts[2].volume}, {ic.parts[3].volume}, {ic.contracts} )";
                }
                else if (ic.tradeType == IronCondor.TradeType.ShortCall_VerticalSpread)
                {
                    sql = @"insert into ironcondors (runID, commission, dateCreated,strikeC,strikeD,expiryC,expiryD, priceC, priceD,callPutC, callPutD, underlyingDate,underlyingPrice, indicatorJSON, tradeType,  volC, volD , contracts )";
                    sql += $" VALUES ({run.id}, {ic.commission}, '{ ic.tradeDate}',  { ic.parts[1].strike}, {ic.parts[0].strike },  ";
                    sql += $"'{ic.parts[1].expiry}', '{ ic.parts[0].expiry}',  {ic.parts[1].price}, {ic.parts[0].price},  ";
                    sql += $" {Convert.ToInt32(ic.parts[1].callPut)}, {Convert.ToInt32(ic.parts[0].callPut)}, '{ic.underlyingDate}', {ic.underlyingPrice}, '{json}', {Convert.ToInt32(ic.tradeType)}, {ic.parts[1].volume}, {ic.parts[0].volume}, {ic.contracts} )";
                }
                else if (ic.tradeType == IronCondor.TradeType.ShortPut_VerticalSpread)
                {
                    sql = @"insert into ironcondors (runID, commission, dateCreated, strikeA,strikeB, expiryA,expiryB, priceA, priceB,  callPutA, callPutB, underlyingDate,underlyingPrice, indicatorJSON, tradeType, volA, volB, contracts )";
                    sql += $" VALUES ({run.id}, {ic.commission}, '{ ic.tradeDate}', { ic.parts[0].strike}, { ic.parts[1].strike},  ";
                    sql += $"'{ic.parts[0].expiry }', '{ ic.parts[1].expiry}',  {ic.parts[0].price}, {ic.parts[1].price},   ";
                    sql += $"{ Convert.ToInt32(ic.parts[0].callPut) }, { Convert.ToInt32(ic.parts[1].callPut)}, '{ic.underlyingDate}', {ic.underlyingPrice}, '{json}', {Convert.ToInt32(ic.tradeType)}, {ic.parts[0].volume}, {ic.parts[1].volume}, {ic.contracts} )";
                }

                ldb.myCommandLocal.CommandText = sql;
                ic.id = ldb.insertWithIdentity(sql);

            }

        }




        public  void addSettlementToDB(Settlement s)
        {
            //string phrase = PhraseManager.getPhrase();

            var sql = $"insert into settlements (settlementType, pl, dateCreated, runID, ironcondorID, underlyingPrice,underlyingDate, contracts, commission) values('{Convert.ToInt32(s.settlementType)}', {s.profitLoss}, '{s.dateCreated}', {run.id}, {s.ironCondorID}, {s.underlyingPrice}, '{s.underlyingDate}', {s.contracts}, {s.commission}); ";
            dbCalls.Append(sql);

            settlements.Add(s);

            //using (LocalDatabase ldb = new LocalDatabase())
            //{
            //    ldb.insertOrUpdateAsync(sql);
            //}
        }

        public  void addRunToDB()
        {

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(run.runSettings);

            string phrase = PhraseManager.getPhrase();

            var sql = $"insert into runs  (name,startDate,endDate, description, barType, startingAccountBalance,endingAccountBalance, runJSON, isDeleted) values('{phrase}','{run.startDate}','{run.endDate}', '{run.description}', {Convert.ToInt32(run.barType)}, {run.startingAccountBalance}, {run.endingAccountBalance}, '{json}', 1)";

            using (LocalDatabase ldb = new LocalDatabase())
            {
                runID = ldb.insertWithIdentity(sql);
                run.id = runID;
            }
        }

        public void endRun()
        {

            //string json = Newtonsoft.Json.JsonConvert.SerializeObject(run.runSettings);
          

            string sql = $"update runs set runLength = {((DateTime.Now - run.runInit).TotalMinutes)}, commissionOverTheRun='{run.commission}',  name='{run.name}',  dbStart ='{run.runInit}', dbEnd ='{DateTime.Now}', endingAccountBalance = {run.endingAccountBalance}, isDeleted = 0 where id =  {run.id} ; ";

            /*
            if (updateSqls != null)
                updateSqls.Append(sql);
            else
            {
                using (LocalDatabase ldb = new LocalDatabase())
                {
                    ldb.insertOrUpdate(sql.ToString());
                }

            }*/

            dbCalls.Append(sql);

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdateAsync(dbCalls.ToString());
            }

            dbCalls = null;

            if (doBankRollYearRandomizer)
                BankRollRandomizer(allTrades,run,settlements); // BankRollRandomizerSettlementsOnly( run, settlements);


            allTrades = new List<IronCondor>();
            activeRuns.Remove(run.id);

            //Console.WriteLine("ENDING RUN " + run.id + " !!!!!!!!!!!!!!!!!!!!!!!!!");
        }


        /*  DONT USE
         * 
         * 
         * public void BankRollRandomizerSettlementsOnly( Run run, List<Settlement> _settlements)
        {
            Dictionary<int, List<Settlement>> settlementsByYear = new Dictionary<int, List<Settlement>>();


            foreach (var s in _settlements)
            {
                int year = s.dateCreated.Year;

                if (settlementsByYear.ContainsKey(year))
                    settlementsByYear[year].Add(s);
                else
                {
                    List<Settlement> ics = new List<Settlement>();
                    ics.Add(s);
                    settlementsByYear.Add(year, ics);
                }
            }

            IEnumerable<IEnumerable<int>> yearPermutations = EnumerableExtensions.GetPermutations(settlementsByYear.Keys, settlementsByYear.Keys.Count);
            List<SettlementOutcome> settlementOutcomes = new List<SettlementOutcome>();


            foreach (var yearPerm in yearPermutations)
            {
                List<Settlement> rearrangedSettlements = new List<Settlement>();

                foreach (var randomYear in yearPerm)
                {
                    rearrangedSettlements.AddRange(settlementsByYear[randomYear]);
                }

                run.availableBalance = run.startingAccountBalance;
                run.endingAccountBalance = run.startingAccountBalance;

                List<Settlement> settlementsClone = JsonConvert.DeserializeObject<List<Settlement>>(Newtonsoft.Json.JsonConvert.SerializeObject(rearrangedSettlements));

                foreach (var s in settlementsClone)
                {
                    decimal originalPL = s.profitLoss / s.contracts;

                    var contracts = GetBetContracts(s.ironCondor.maxRisk, run);

                    s.profitLoss = originalPL * contracts;

                    run.availableBalance += s.profitLoss - s.commission;
                    run.endingAccountBalance += s.profitLoss - s.commission;

                }

                SettlementOutcome d = new SettlementOutcome();
                d.endingAccountBalance = run.endingAccountBalance;
                d.years = yearPerm.ToList();
                settlementOutcomes.Add(d);


            }

            decimal minBalance = settlementOutcomes.Min(x => x.endingAccountBalance);
            decimal maxBalance = settlementOutcomes.Max(x => x.endingAccountBalance);
            List<decimal> balances = settlementOutcomes.Select(x => x.endingAccountBalance).ToList();
            decimal avgBalance = balances.Average();

            string sql = $"update runs set minBalance = {minBalance}, maxBalance='{maxBalance}',  avgBalance='{avgBalance}', permutations = '{yearPermutations.Count()}' where id =  {run.id} ; ";

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdate(sql);
            }


        }*/

        public void BankRollRandomizer(List<IronCondor> _allTrades, Run run, List<Settlement> _settlements)
        {
            

            //BANKROLL RANDOMIZER
            Dictionary<int, List<IronCondor>> condorsByYear = new Dictionary<int, List<IronCondor>>();

           
            foreach (var ic in _allTrades)
            {
                int year = ic.tradeDate.Year;
                IronCondor clone = (IronCondor)ic.Clone();

                if (condorsByYear.ContainsKey(year))
                    condorsByYear[year].Add(clone);
                else
                {
                    List<IronCondor> ics = new List<IronCondor>();
                    ics.Add(clone);
                    condorsByYear.Add(year, ics);
                }
            }


            //List<int> randomYears = settlementsByYear.Keys.Shuffle().Take(6).ToList();

            IEnumerable<IEnumerable<int>> yearPermutations = EnumerableExtensions.GetPermutations(condorsByYear.Keys, condorsByYear.Keys.Count);
            List<SettlementOutcome> settlementOutcomes = new List<SettlementOutcome>();

 
          

            foreach (var yearPerm in yearPermutations)
            {
                List<IronCondor> rearrangedCondors = new List<IronCondor>();

                foreach (var randomYear in yearPerm)
                {
                    rearrangedCondors.AddRange(condorsByYear[randomYear].ConvertAll(c => c.Clone()));
                }

                List<IronCondor> fakeCurrHoldings = new List<IronCondor>();

                run.availableBalance = run.startingAccountBalance;
                run.endingAccountBalance = run.startingAccountBalance;

                List<Settlement> settlements = _settlements.ConvertAll(settle => settle.Clone(rearrangedCondors.First(x=>x.id == settle.ironCondorID)));
                //List<IronCondor> allTrades = _allTrades.ConvertAll(trade => trade.Clone());
       

                bool firstRun = true;
                decimal lastPL = 0;
                int lastWinStreak = 0;
                int lastLossStreak = 0;
                int maxLossStreak = 0;
                int maxWinStreak = 0;

                Dictionary<DateTime, decimal> allDrawDowns = new Dictionary<DateTime, decimal>();


                decimal maxDrawDown = 0;
                decimal lastDrawDown = 0;
                decimal startOfDecline = 0;
                decimal endOfDecline = 0;

                foreach (var condor in rearrangedCondors)
                {

                    List<long> removeIDs = new List<long>();

                    if (!firstRun)
                    {

                        //List<IronCondor> clonedHoldings = JsonConvert.DeserializeObject<List<IronCondor>>(Newtonsoft.Json.JsonConvert.SerializeObject(fakeCurrHoldings));


                        for (int x = 0; x < fakeCurrHoldings.Count; x++)
                        {
                            IronCondor ic = fakeCurrHoldings[x];

                            if (ic.settlement != null && condor.tradeDate >= ic.settlement.dateCreated ) //
                            {
                                bankrollSettle(removeIDs, allDrawDowns, fakeCurrHoldings, ic, ref endOfDecline, ref lastDrawDown, ref maxDrawDown, ref lastPL, ref lastWinStreak, ref maxWinStreak, ref lastLossStreak, ref maxLossStreak, ref startOfDecline);
                            }
                        }

                        foreach(var id in removeIDs)
                            fakeCurrHoldings.RemoveAll(x => x.id == id);
                    }

                    firstRun = false;

                    int contracts = GetSetBetContracts(condor, condor.maxRisk, run);
                    condor.contracts = contracts;

                    fakeCurrHoldings.Add(condor);

                    run.availableBalance -= 20;
                    run.availableBalance -= Convert.ToDecimal(condor.maxRisk);

                    run.endingAccountBalance -= 20;

                    if (condor.tradeType == IronCondor.TradeType.IronCondor)
                    {
                        run.endingAccountBalance -= (1 * contracts * 4);
                        run.availableBalance -= (1 * contracts * 4);
                    }
                    else
                    {
                        run.endingAccountBalance -= (1 * contracts * 2);
                        run.availableBalance -= (1 * contracts * 2);
                    }
                }


                foreach (var leftover in fakeCurrHoldings)
                {
                    Settlement s = settlements.FirstOrDefault(x => x.ironCondorID == leftover.id);

                    bankrollSettle(new List<long>(), allDrawDowns, fakeCurrHoldings, leftover, ref endOfDecline, ref lastDrawDown, ref maxDrawDown, ref lastPL, ref lastWinStreak, ref maxWinStreak, ref lastLossStreak, ref maxLossStreak, ref startOfDecline);

                }

                //foreach (var id in removeLeftoverIDs)
                 //   fakeCurrHoldings.RemoveAll(x => x.id == id);


                SettlementOutcome d = new SettlementOutcome();
                d.endingAccountBalance = run.endingAccountBalance;
                d.maxWinStreak = maxWinStreak;
                d.maxLossStreak = maxLossStreak;
                d.maxYearlyDrawDown = maxDrawDown;
                d.drawDowns = allDrawDowns;
                d.years = yearPerm.ToList();
                settlementOutcomes.Add(d);
            }

            decimal minBalance = settlementOutcomes.Min(x => x.endingAccountBalance);
            decimal maxBalance = settlementOutcomes.Max(x => x.endingAccountBalance);
            List<decimal> balances = settlementOutcomes.Select(x => x.endingAccountBalance).ToList();
            decimal avgBalance = balances.Average();

            int maxMaxLossStreak = settlementOutcomes.Max(x => x.maxLossStreak);
            int maxMaxWinStreak = settlementOutcomes.Max(x => x.maxWinStreak);

            decimal maxMaxDrawDown = settlementOutcomes.Max(x => x.maxYearlyDrawDown);

            List<int> losses = settlementOutcomes.Select(x => x.maxLossStreak).ToList();
            int averageLossStreak = Convert.ToInt32(losses.Average());

            List<int> wins = settlementOutcomes.Select(x => x.maxWinStreak).ToList();
            int averageWinStreak = Convert.ToInt32(wins.Average());

            List<decimal> drawdowns = settlementOutcomes.Select(x => x.maxYearlyDrawDown).ToList();
            decimal averageDrawDown = drawdowns.Average();


            string sql = $"update runs set minBalance = {minBalance}, maxBalance='{maxBalance}',  avgBalance='{avgBalance}', permutations = '{yearPermutations.Count()}', maxLossStreak ={maxMaxLossStreak}, maxWinStreak={maxMaxWinStreak}, averageLossStreak={averageLossStreak}, averageWinStreak={averageWinStreak}, maxDrawDown={maxMaxDrawDown}, averageDrawDown={averageDrawDown} where id =  {run.id} ; ";

            /*
            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdate(sql);
            }*/

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdateAsync(sql);
            }

        }


        public void bankrollSettle(List<long> removeIDs, Dictionary<DateTime, decimal> allDrawDowns, List<IronCondor> fakeCurrHoldings, IronCondor ic, ref decimal endOfDecline, ref decimal lastDrawDown, ref decimal maxDrawDown,  ref decimal lastPL, ref int lastWinStreak, ref int maxWinStreak, ref int lastLossStreak, ref int maxLossStreak, ref decimal startOfDecline)
        {

            
            Settlement s = settlements.FirstOrDefault(x => x.ironCondorID == ic.id);

            if (s != null)
            {

                decimal originalPL = s.profitLoss / s.contracts;

                //s.profitLoss = originalPL * ic.contracts;

                decimal newPL = originalPL * ic.contracts;

                if (newPL > 0 && lastPL >= 0)
                {
                    lastWinStreak++;
                    if (lastWinStreak > maxWinStreak)
                        maxWinStreak = lastWinStreak;
                }
                else if (newPL < 0 && lastPL <= 0)
                {
                    lastLossStreak++;
                    if (lastLossStreak > maxLossStreak)
                        maxLossStreak = lastLossStreak;
                }
                else if (newPL > 0 && lastPL < 0)
                    lastLossStreak = 0;
                else if (newPL < 0 && lastPL > 0)
                    lastWinStreak = 0;


                //drawdown
                if (lastPL >= 0 && newPL < 0)
                {
                    startOfDecline = run.endingAccountBalance;
                }
                else if (lastPL < 0 && newPL > 0)
                {
                    endOfDecline = run.endingAccountBalance;
                    lastDrawDown = (startOfDecline - endOfDecline) / startOfDecline;

                    if (lastDrawDown > maxDrawDown)
                        maxDrawDown = lastDrawDown;

                    try
                    {
                        allDrawDowns.Add(s.dateCreated, lastDrawDown);
                    }
                    catch (Exception e)
                    {

                    }

                }

                run.endingAccountBalance += newPL;

                run.availableBalance += newPL + Convert.ToDecimal(ic.maxRisk);

                removeIDs.Add(ic.id);

                lastPL = newPL;
            }
        }

    }
}
