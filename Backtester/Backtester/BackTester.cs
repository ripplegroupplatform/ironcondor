using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore;
using Google.Type;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Skender.Stock.Indicators;
using DateTime = System.DateTime;
using DayOfWeek = System.DayOfWeek;

namespace Backtester
{
    public static class BackTester
    {
        public static int activeThreads = 0;
        public static bool watchMode = true;

        public static Dictionary<long,Run> activeRuns = new Dictionary<long, Run>();




        static void Main(string[] args)
        {
            ThreadPool.SetMaxThreads(1000,1000);
            //StringBuilder updateSqls = new StringBuilder("", 1 * 200);
            PhraseManager.loadPhraseLists();

            DataHelper dataHelper;

            int activeTestID = 0;

            try
            {
                if (watchMode)
                {
                    while (1 == 1)
                    {


                        

                        //Run run = getNextRun(activeRuns);

                        /*
                        String updates = updateSqls.ToString();
                        if(!String.IsNullOrEmpty(updates))
                        {
                            using (LocalDatabase ldb = new LocalDatabase())
                            {
                                ldb.insertOrUpdate(updates);
                            }
                        }
                        updateSqls = new StringBuilder("", 10000 * 200);
                        */

                        //GetNextRuns(activeRuns, 100000 - ThreadPool.PendingWorkItemCount);

                        if (activeTestID > 0 && activeRuns.Count == 0)
                        {
                            Console.WriteLine("COMPLETING TEST:" + activeTestID);
                            dynamic testStats = getTestStats(activeTestID);
                            updateTest(testStats, activeTestID);
                           
                        }

                        if (activeRuns.Count == 0)
                        {
                            activeTestID = getNextTest();
                            dataHelper = new DataHelper();
                            Console.WriteLine("FOUND TEST:" + activeTestID);
                            clearTest(activeTestID);

                            foreach (var key in activeRuns.Keys)
                            {
                                //BackTestBrain b = new BackTestBrain(ref updateSqls);
                                BackTestBrain b = new BackTestBrain(activeRuns, dataHelper);

                                b.run = activeRuns[key];

                                if (b.run.runLoaded)
                                {
                                    ThreadPool.QueueUserWorkItem(new WaitCallback(b.Run));
                                }

                            }
                        }
                        
                        Thread.Sleep(60000);
                    }
                }
                else
                {
                    dataHelper = new DataHelper();
                    BackTestBrain brain = new BackTestBrain(activeRuns, dataHelper);
                    brain.RunSingleRun();
                }
            }
            catch(Exception bigE)
            {
                Console.WriteLine(bigE.ToString());
            }
        }

        public static Run getNextRun(List<Run> _incRuns)
        {
            Run r = new Run();

            List<long> ids = _incRuns.Select(r => r.id).ToList();

            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = "SELECT top 1 * FROM runs r WHERE r.runLength IS NULL AND r.isDeleted = 0 AND r.autoRun = 1 AND ( r.testID IS NULL OR ( SELECT COUNT ( id ) from tests where r.testid = id and isDeleted = 0) > 0 )";
               
                if (ids.Count > 0)
                    ldb.myCommandLocal.CommandText += " AND ID NOT IN (" + string.Join(",", ids) + ") ";

                ldb.myCommandLocal.CommandText += "ORDER BY priority DESC";

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                while (ldb.myReaderLocal.Read())
                {
                    r.startDate = Convert.ToDateTime(ldb.myReaderLocal["startDate"].ToString());
                    r.endDate = Convert.ToDateTime(ldb.myReaderLocal["endDate"].ToString());
                    r.startingAccountBalance = decimal.Parse(ldb.myReaderLocal["startingAccountBalance"].ToString());
                    r.runSettings = Newtonsoft.Json.JsonConvert.DeserializeObject(ldb.myReaderLocal["runJSON"].ToString());
                    r.name = PhraseManager.getPhrase();
                    r.barType = Backtester.Run.BarType.Hourly;
                    r.runLoaded = true;
                    r.endingAccountBalance = r.startingAccountBalance;
                    try
                    {
                        r.testID = Convert.ToInt32(ldb.myReaderLocal["testID"].ToString());
                    }
                    catch(Exception noT)
                    {


                    }


                    r.runInit = DateTime.Now;
                    r.id = Convert.ToInt64(ldb.myReaderLocal["id"].ToString());
                  
                }
            }

            return r;

        }


        public static dynamic getTestStats(int testID)
        {
            dynamic d = new ExpandoObject();


            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = "SELECT  (select count(id) from runs where testid= " + testID + ") as numRuns, (select min(endingAccountBalance) from runs where testid= " + testID + ") as minBal,  (select max(coalesce(avgBalance,endingAccountBalance)) from runs where testid= " + testID + ") as maxBal   from tests where id = " + testID;
                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                if (ldb.myReaderLocal.Read())
                {
                    d.numRuns = Convert.ToInt32(ldb.myReaderLocal["numRuns"].ToString());
                    d.minBal = Convert.ToDouble(ldb.myReaderLocal["minBal"].ToString());
                    d.maxBal = Convert.ToDouble(ldb.myReaderLocal["maxBal"].ToString());
                }
            }


            return d;
        }


        public static int getNextTest()
        {
            int testID = 0;

            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = "SELECT top 1 t.* FROM tests t where t.isDeleted = 0 and t.isComplete =0 ORDER BY priority DESC";
                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                if(ldb.myReaderLocal.Read())
                {
                    testID = Convert.ToInt32(ldb.myReaderLocal["id"].ToString());
                }
            }

            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = "update tests set dbStart = getDate() where id = " + testID;
                ldb.myCommandLocal.ExecuteNonQuery();
            }

            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = "SELECT  * FROM runs where testID = " + testID;

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                while (ldb.myReaderLocal.Read())
                {
                    Run r = new Run();
                    r.startDate = Convert.ToDateTime(ldb.myReaderLocal["startDate"].ToString());
                    r.endDate = Convert.ToDateTime(ldb.myReaderLocal["endDate"].ToString());
                    r.startingAccountBalance = decimal.Parse(ldb.myReaderLocal["startingAccountBalance"].ToString());
                    r.runSettings = Newtonsoft.Json.JsonConvert.DeserializeObject(ldb.myReaderLocal["runJSON"].ToString());
                    r.name = PhraseManager.getPhrase();
                    r.barType = Backtester.Run.BarType.Hourly;
                    r.runLoaded = true;
                    r.endingAccountBalance = r.startingAccountBalance;
                    r.testID = testID;
                    r.runInit = DateTime.Now;
                    r.id = Convert.ToInt64(ldb.myReaderLocal["id"].ToString());
                    activeRuns.Add(r.id,r);
                }
            }

            return testID;

        }

        public static void GetNextRuns(List<Run> runList, long limit)
        {
          
            List<long> ids = runList.Select(r => r.id).ToList();

            try
            {

                using (LocalDatabase ldb = new LocalDatabase())
                {

                    ldb.myCommandLocal.CommandText = "SELECT TOP " + limit + " * FROM runs  r WHERE r.runLength IS NULL AND r.isDeleted = 0 AND r.autoRun = 1 AND ( r.testID IS NULL OR ( SELECT COUNT ( id ) from tests where r.testid = id and isDeleted = 0) > 0 )";

                    if (ids.Count > 0)
                        ldb.myCommandLocal.CommandText += " AND ID NOT IN (" + string.Join(",", ids) + ") ";

                    ldb.myCommandLocal.CommandText += "ORDER BY priority DESC";

                    ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                    while (ldb.myReaderLocal.Read())
                    {
                        Run r = new Run();
                        r.startDate = Convert.ToDateTime(ldb.myReaderLocal["startDate"].ToString());
                        r.endDate = Convert.ToDateTime(ldb.myReaderLocal["endDate"].ToString());
                        r.startingAccountBalance = decimal.Parse(ldb.myReaderLocal["startingAccountBalance"].ToString());
                        r.runSettings = Newtonsoft.Json.JsonConvert.DeserializeObject(ldb.myReaderLocal["runJSON"].ToString());
                        r.name = PhraseManager.getPhrase();
                        r.barType = Backtester.Run.BarType.Hourly;
                        r.runLoaded = true;
                        r.endingAccountBalance = r.startingAccountBalance;
                        try
                        {
                            r.testID = Convert.ToInt32(ldb.myReaderLocal["testID"].ToString());
                        }
                        catch(Exception e)
                        {

                        }

                        r.id = Convert.ToInt64(ldb.myReaderLocal["id"].ToString());
                        runList.Add(r);
                    }
                }

            }
            catch(Exception timeoutE)
            {
                Console.WriteLine("DB GET RUN ERROR: " + timeoutE.ToString());
            }

        }

        public static void clearTest(int id)
        {
            string sql = $" delete from ironcondors where runID IN (select id from runs where testID = " + id + "); delete from settlements where runID IN (select id from runs where testID = " + id + "); update runs set endingAccountBalance=0, runlength=null where testid = " + id;
            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdate(sql.ToString());
            }
        }

        public static void updateTest(dynamic stats, int id)
        {
            string sql = $"update tests set isComplete = 1, dbEnd = getDate(), numRuns = " + stats.numRuns + ", minBal=" + stats.minBal + ", maxBal = " + stats.maxBal + " where id =  " + id;
            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.insertOrUpdate(sql.ToString());
            }
        }
    }
}

