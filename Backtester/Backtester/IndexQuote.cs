using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skender.Stock.Indicators;

namespace Backtester
{
  public class IndexQuote : IQuote
  {
    public DateTime Date { get; set; }
    public decimal Open { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public decimal Close { get; set; }
    public decimal Volume { get; set; }

    public IndexQuote()
    {

    }
    public IndexQuote(DateTime _date, decimal _open, decimal _high, decimal _low, decimal _close, decimal _volume)
    {
      Date = _date;
      Open = _open;
      High = _high;
      Low = _low;
      Close = _close;
      Volume = _volume;

    }

  }
}
