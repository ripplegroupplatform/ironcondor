using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skender.Stock.Indicators;

namespace Backtester
{

  public enum CallPut
  {
    CALL = 'C',
    PUT = 'P'
  }

  public class OptionQuote : IQuote
  {
    public OptionQuote()
    {


    }


    public OptionQuote(DateTime _date, decimal _open, decimal _high, decimal _low, decimal _close, decimal _volume)
    {
      Date = _date;
      Open = _open;
      High = _high;
      Low = _low;
      Close = _close;
      Volume = _volume;

      OpenBid = 0;
      OpenAsk = 0;
      OpenTrade = 0;
      OpenBidVolume = 0;
      OpenAskVolume = 0;
      OpenTradeVolume = 0;

      HighBid = 0;
      HighAsk = 0;
      HighTrade = 0;
      HighBidVolume = 0;
      HighAskVolume = 0;
      HighTradeVolume = 0;

      LowBid = 0;
      LowAsk = 0;
      LowTrade = 0;
      LowBidVolume = 0;
      LowAskVolume = 0;
      LowTradeVolume = 0;

      CloseBid = 0;
      CloseAsk = 0;
      CloseTrade = 0;
      CloseBidVolume = 0;
      CloseAskVolume = 0;
      CloseTradeVolume = 0;

    }

    public DateTime JustTheDate { get; set; }

    public DateTime Date { get; set; }
    public decimal Open { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public decimal Close { get; set; }
    public decimal Volume { get; set; }

    // custom properties
    public CallPut CallPut { get; set; }
    public int Strike { get; set; }
    public DateTime Expiry { get; set; }

    public int TotalTrades { get; set; }

    public decimal OpenAsk { get; set; }
    public decimal HighAsk { get; set; }
    public decimal LowAsk { get; set; }
    public decimal CloseAsk { get; set; }

    public decimal OpenBid { get; set; }
    public decimal HighBid { get; set; }
    public decimal LowBid { get; set; }
    public decimal CloseBid { get; set; }

    public decimal OpenTrade { get; set; }
    public decimal HighTrade { get; set; }
    public decimal LowTrade { get; set; }
    public decimal CloseTrade { get; set; }


    public int OpenAskVolume { get; set; }
    public int HighAskVolume { get; set; }
    public int LowAskVolume { get; set; }
    public int CloseAskVolume { get; set; }

    public decimal OpenTradeVolume { get; set; }
    public decimal HighTradeVolume { get; set; }
    public decimal LowTradeVolume { get; set; }
    public decimal CloseTradeVolume { get; set; }

    public int OpenBidVolume { get; set; }
    public int HighBidVolume { get; set; }
    public int LowBidVolume { get; set; }
    public int CloseBidVolume { get; set; }

    public decimal MidPoint { get; set; }

  }
}
