﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
    public class SettlementOutcome
    {
        public decimal endingAccountBalance;
        public List<int> years;

        public decimal maxMonthlyDrawDown;
        public decimal maxYearlyDrawDown;
        public decimal averageMonthlyDrawDown;
        public decimal averageYearlyDrawDown;

        public decimal maxDrawDown;
        public decimal averageDrawDown;

        public int maxLossStreak;
        public int maxWinStreak;

        public Dictionary<DateTime, decimal> drawDowns;

    }
}
