using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Backtester
{
    public class Settlement
    {
        public enum SettlementType
        {
            ExpiredOTM = 1,
            ExpiredITM = 2,
            Exited = 3
        }


        public SettlementType settlementType;
        public DateTime dateCreated = DateTime.Now;
        public decimal profitLoss;
        public int id;
        public long ironCondorID;
        public decimal underlyingPrice;
        public DateTime underlyingDate;
        public int contracts;
        public decimal commission;

        public IronCondor ironCondor;

        public Settlement Clone(IronCondor ic)
        {
            Settlement retVal = new Settlement();
            retVal.ironCondor = ic;

            retVal.settlementType = settlementType;
            retVal.dateCreated = dateCreated;
            retVal.profitLoss = profitLoss;
            retVal.id = id;
            retVal.ironCondorID = ironCondorID;
            retVal.underlyingPrice = underlyingPrice;
            retVal.underlyingDate = underlyingDate;
            retVal.contracts = contracts;
            retVal.commission = commission;

            return retVal;
        }

    }
}
