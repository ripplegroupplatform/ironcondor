using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Skender.Stock.Indicators;

namespace Backtester
{
    public  class DataHelper
    {
        private  bool isLoaded = false;
        public  decimal MidPointAdjuster = 1;

        public  IEnumerable<IQuote> myIndexQuotes;
        public  IEnumerable<IQuote> vixQuotes;
        public  IEnumerable<OptionQuote> myQuotes;
        public  List<OptionQuote> expiryDates;
        public  List<DateTime> quoteDates;

        public  ConcurrentDictionary<DateTime, IndexQuote> rutLastCloseDict = new ConcurrentDictionary<DateTime, IndexQuote>();
        public  ConcurrentDictionary<DateTime, IndexQuote> vixLastCloseDict = new ConcurrentDictionary<DateTime, IndexQuote>();

        public  ConcurrentDictionary<DateTime, List<OptionQuote>> dataForDayDict = new ConcurrentDictionary<DateTime, List<OptionQuote>>();
        public  ConcurrentDictionary<string, OptionQuote> expiryDateDict = new ConcurrentDictionary<string, OptionQuote>();
        public  ConcurrentDictionary<string, OptionQuote> quoteDayExpiryDateDict = new ConcurrentDictionary<string, OptionQuote>();
        public  ConcurrentDictionary<string, AdxResult> AdxOnDateDict = new ConcurrentDictionary<string, AdxResult>();
        public  ConcurrentDictionary<string, BollingerBandsResult> BollingerOnDateDict = new ConcurrentDictionary<string, BollingerBandsResult>();
        public  ConcurrentDictionary<string, MacdResult> MACDOnDateDict = new ConcurrentDictionary<string, MacdResult>();
        public  ConcurrentDictionary<string, RsiResult> RSIOnDateDict = new ConcurrentDictionary<string, RsiResult>();

        public  IndexQuote GetOrAddRutLastClose(DateTime key)
        {
            IndexQuote retVal = null;
            if (!rutLastCloseDict.TryGetValue(key, out retVal))
            {
                rutLastCloseDict.TryAdd(key, getLastClose(key));
                rutLastCloseDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }
        public  IndexQuote GetOrAddVixLastClose(DateTime key)
        {
            IndexQuote retVal = null;
            if (!vixLastCloseDict.TryGetValue(key, out retVal))
            {
                vixLastCloseDict.TryAdd(key, getVixLastClose(key));
                vixLastCloseDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  IndexQuote getVixLastClose(DateTime date)
        {
            bool foundLastClose = false;
            IndexQuote lastClose = new IndexQuote();

            DateTime lastCloseDate = date.AddDays(-1);

            while (!foundLastClose)
            {
                lastClose = (IndexQuote)vixQuotes.FirstOrDefault(x => x.Date.Date == lastCloseDate.Date);
                if (lastClose != null)
                {
                    foundLastClose = true;
                }

                lastCloseDate = lastCloseDate.AddDays(-1);

            }

            return lastClose;
        }

        public  List<OptionQuote> GetDataForDayDict(DateTime key)
        {
            List<OptionQuote> retVal = null;
            if (!dataForDayDict.TryGetValue(key, out retVal))
            {
                dataForDayDict.TryAdd(key, getDataForDay(key).OrderBy(x => x.Date).ToList());
                dataForDayDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  OptionQuote GetDataForExpiryDict(DateTime currDate, int daysOut)
        {
            OptionQuote retVal = null;

            string key = currDate.ToString();

            if (!expiryDateDict.TryGetValue(key, out retVal))
            {
                expiryDateDict.TryAdd(key, expiryDates.Where(x => x.Expiry >= currDate.AddDays(daysOut) && x.Date.Date == currDate.Date).OrderBy(x => x.Expiry).FirstOrDefault());
                expiryDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  OptionQuote GetQuoteDayExpiryDateDict(int strike, int strikeB, CallPut callPut, DateTime expDate, DateTime currDate)
        {
            OptionQuote retVal = null;

            string key = currDate.ToString() + expDate.ToString() + callPut + strike;

            if (!quoteDayExpiryDateDict.TryGetValue(key, out retVal))
            {
                var myDayQuotes = GetDataForDayDict(currDate);

                var quoteC = myDayQuotes.OrderBy(x => x.Strike).FirstOrDefault(x =>
                                x.Strike >= (strike) && x.Strike != strikeB && x.CallPut == callPut && x.Expiry == expDate &&
                                (x.Date == currDate));

                quoteDayExpiryDateDict.TryAdd(key, quoteC);
                quoteDayExpiryDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        

        public  AdxResult GetAdxOnDate(DateTime currDate, int length)
        {
            AdxResult retVal = null;

            string key = length + currDate.ToString();

            if (!AdxOnDateDict.TryGetValue(key, out retVal))
            {
                AdxOnDateDict.TryAdd(key, myIndexQuotes.GetAdx(length).FirstOrDefault(x => x.Date.Date == currDate.Date.Date));
                AdxOnDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  RsiResult GetRSIOnDate(DateTime currDate, int length)
        {
            RsiResult retVal = null;

            string key = length + currDate.ToString();

            if (!RSIOnDateDict.TryGetValue(key, out retVal))
            {
                RSIOnDateDict.TryAdd(key, myIndexQuotes.GetRsi(length).FirstOrDefault(x => x.Date.Date == currDate.Date.Date));
                RSIOnDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  BollingerBandsResult GetBollingerOnDate(DateTime currDate, int length, int sd)
        {
            BollingerBandsResult retVal = null;

            string key = length + currDate.ToString() + sd;

            if (!BollingerOnDateDict.TryGetValue(key, out retVal))
            {
                BollingerOnDateDict.TryAdd(key, myIndexQuotes.GetBollingerBands(length, sd).FirstOrDefault(x => x.Date.Date == currDate.Date.Date));
                BollingerOnDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }

        public  MacdResult GetMACDOnDate(DateTime currDate)
        {
            MacdResult retVal = null;

            string key = currDate.ToString();

            if (!MACDOnDateDict.TryGetValue(key, out retVal))
            {
                MACDOnDateDict.TryAdd(key, myIndexQuotes.GetMacd().FirstOrDefault(x => x.Date.Date == currDate.Date.Date));
                MACDOnDateDict.TryGetValue(key, out retVal);
            }

            return retVal;
        }
        public  IndexQuote getLastClose(DateTime date)
        {
            bool foundLastClose = false;
            IndexQuote lastClose = new IndexQuote();

            DateTime lastCloseDate = date.AddDays(-1);

            while (!foundLastClose)
            {
                lastClose = (IndexQuote)myIndexQuotes.FirstOrDefault(x => x.Date.Date == lastCloseDate.Date);
                if (lastClose != null)
                {
                    foundLastClose = true;
                }

                lastCloseDate = lastCloseDate.AddDays(-1);
            }

            return lastClose;
        }

        public  void setupDBData(Run run)
        {
            if (!isLoaded)
            {
                quoteDates = getDistinctQuoteDates(run.startDate, run.endDate);
                expiryDates = getDistinctExpiryDates(run.startDate, run.endDate);
                //myQuotes = DataHelper.getDataForDay(run.startDate,run.endDate).OrderBy(x => x.Date).ToList();


                DataTable rutIndex = loadData(@"C:\Data\FOR QUANTCONNECT\rutCloses.csv");
                DataTable vixIndex = loadData(@"C:\Data\FOR QUANTCONNECT\vixCloses.csv");

                myIndexQuotes = GetIndexFromFeed(rutIndex.Rows);
                vixQuotes = GetIndexFromFeed(vixIndex.Rows);
                isLoaded = true;
            }
        }

        public  List<DateTime> getDistinctQuoteDates(DateTime startDay, DateTime endDay)
        {
            List<DateTime> quoteDays = new List<DateTime>();


            string sql = "SELECT CONVERT(DATE, dataDate) AS dataDate FROM hourlyData where dataDate >='" + startDay.Date.ToShortDateString() + "' and dataDate  <='" + endDay.Date.ToShortDateString() + " 23:59:59' GROUP BY CONVERT(DATE, dataDate) order by dataDate";

            using (LocalDatabase ldb = new LocalDatabase())
            {

                ldb.myCommandLocal.CommandText = sql;
                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                while (ldb.myReaderLocal.Read())
                {

                    quoteDays.Add(Convert.ToDateTime(ldb.myReaderLocal["dataDate"]));

                }

            }

            return quoteDays;
        }

        public  List<OptionQuote> getDistinctExpiryDates(DateTime startDay, DateTime endDay)
        {
            List<OptionQuote> quoteDays = new List<OptionQuote>();

            string sql = "SELECT CONVERT(DATE, dataDate) AS dataDate, expiry FROM hourlyData where dataDate >='" + startDay.Date.ToShortDateString() + "' and dataDate  <='" + endDay.Date.ToShortDateString() + " 23:59:59' GROUP BY CONVERT(DATE, dataDate), expiry order by datadate, expiry ";

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.myCommandLocal.CommandText = sql;
                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();

                while (ldb.myReaderLocal.Read())
                {
                    OptionQuote q = new OptionQuote();
                    q.Expiry = Convert.ToDateTime(ldb.myReaderLocal["expiry"]);
                    q.Date = Convert.ToDateTime(ldb.myReaderLocal["dataDate"]);
                    quoteDays.Add(q);
                }

            }

            //quoteDays = quoteDays.GroupBy(m => new { m.Expiry, m.JustTheDate }).Select(group => group.First()).ToList();

            return quoteDays;
        }

        public  List<OptionQuote> getDataForDays(DateTime day, DateTime endDay)
        {
            List<OptionQuote> retVal = new List<OptionQuote>();

            using (LocalDatabase ldb = new LocalDatabase())
            {
                var sql = $"select * from hourlyData where dataDate <='{endDay.Date.ToShortDateString()} 23:59:59' AND dataDate >='{day.Date.ToShortDateString()}'";

                DataSet dataset = new DataSet();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(sql, ldb.myConnectionLocal);
                adapter.Fill(dataset, "hourlyData");

                foreach(DataRow row in dataset.Tables["hourlyData"].Rows)
                {
                    OptionQuote q = fillQuoteFromDBData(row);

                    retVal.Add(q);
                }
            }

            return retVal;
        }

        public  List<OptionQuote> getDataForDay(DateTime day)
        {
            List<OptionQuote> retVal = new List<OptionQuote>();

            using (LocalDatabase ldb = new LocalDatabase())
            {
                var sql = $"select * from hourlyData where dataDate = '" + day + "'";

                DataSet dataset = new DataSet();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = new SqlCommand(sql, ldb.myConnectionLocal);
                adapter.Fill(dataset, "hourlyData");

                foreach (DataRow row in dataset.Tables["hourlyData"].Rows)
                {
                    OptionQuote q = fillQuoteFromDBData(row);

                    retVal.Add(q);
                }
            }

            return retVal;
        }



        public  void fillInData(string optionsfile)
        {

            DataTable rutIndex = loadData(@"C:\Data\FOR QUANTCONNECT\rutCloses.csv");
            DataTable vixIndex = loadData(@"C:\Data\FOR QUANTCONNECT\vixCloses.csv");

            myIndexQuotes = GetIndexFromFeed(rutIndex.Rows);
            vixQuotes = GetIndexFromFeed(vixIndex.Rows);

            DataTable optionsData = loadData(optionsfile);

            myQuotes = GetHistoryFromFeed(optionsData.Rows).OrderBy(x => x.Date);

            //distinct expiry dates for each day
            expiryDates = myQuotes.GroupBy(m => new { m.Expiry, m.JustTheDate }).Select(group => group.First()).ToList();

            //distinct expiry dates for each day
            quoteDates = myQuotes.Select(x => x.Date.Date).Distinct().ToList();


        }

         IEnumerable<OptionQuote> GetHistoryFromFeed(DataRowCollection feedRows)
        {
            List<OptionQuote> retVal = new List<OptionQuote>();



            for (var x = 0; x < feedRows.Count; x++)
            //foreach (DataRow row in feedRows)
            {
                DataRow row = feedRows[x];

                if (row["Date"].ToString() != "Date")
                {

                    DateTime date = DateTime.Parse(row["Date"].ToString());


                    OptionQuote q = new OptionQuote(date, 0, 0, 0, 0, 0);

                    if (row["Type"].ToString() == "PUT")
                        q.CallPut = CallPut.PUT;
                    else
                        q.CallPut = CallPut.CALL;

                    q.JustTheDate = date.Date;

                    q.Strike = Convert.ToInt32(Convert.ToDecimal(row["Strike"]));
                    //q.TotalTrades = Convert.ToInt32(row["TotalTrades"].ToString());

                    // int vol;
                    // Int32.TryParse(row["Volume"].ToString(), out vol);



                    q.Expiry = DateTime.Parse(row["Expiry"].ToString());

                    decimal output;
                    int outputInt;

                    //OPEN
                    // Decimal.TryParse(row["OpenBidPrice"].ToString(), out output);
                    // q.OpenBid = output;

                    // Int32.TryParse(row["OpenBidSize"].ToString(), out outputInt);
                    // q.OpenBidVolume = outputInt;

                    // Decimal.TryParse(row["OpenAskPrice"].ToString(), out output);
                    // q.OpenAsk = output;

                    // Int32.TryParse(row["OpenTradeSize"].ToString(), out outputInt);
                    //  q.OpenAskVolume = outputInt;

                    //HIGH
                    // Decimal.TryParse(row["HighBidPrice"].ToString(), out output);
                    // q.HighBid = output;

                    // Int32.TryParse(row["HighBidSize"].ToString(), out outputInt);
                    // q.HighBidVolume = outputInt;

                    //  Decimal.TryParse(row["HighAskPrice"].ToString(), out output);
                    // q.HighAsk = output;

                    // Int32.TryParse(row["HighAskSize"].ToString(), out outputInt);
                    //  q.HighAskVolume = outputInt;

                    //LOW
                    // Decimal.TryParse(row["LowBidPrice"].ToString(), out output);
                    // q.LowBid = output;

                    //  Int32.TryParse(row["LowBidSize"].ToString(), out outputInt);
                    //  q.LowBidVolume = outputInt;

                    // Decimal.TryParse(row["LowAskPrice"].ToString(), out output);
                    // q.LowAsk = output;

                    // Int32.TryParse(row["LowAskSize"].ToString(), out outputInt);
                    // q.LowAskVolume = outputInt;

                    //CLOSE
                    Decimal.TryParse(row["Bid"].ToString(), out output);
                    q.CloseBid = output;

                    Int32.TryParse(row["Close Vol"].ToString(), out outputInt);
                    q.CloseBidVolume = outputInt;

                    Decimal.TryParse(row["Ask"].ToString(), out output);
                    q.CloseAsk = output;

                    Int32.TryParse(row["Ask Vol"].ToString(), out outputInt);
                    q.CloseAskVolume = outputInt;

                    q.Volume = q.CloseBidVolume + q.CloseAskVolume;

                    //Decimal.TryParse(row["CloseTradePrice"].ToString(), out output);
                    // q.CloseTrade = output;

                    // Int32.TryParse(row["CloseTradeSize"].ToString(), out outputInt);
                    // q.CloseTradeVolume = outputInt;

                    q.Open = (q.OpenAsk + q.OpenBid) / 2 * MidPointAdjuster;
                    q.High = (q.HighAsk + q.HighBid) / 2 * MidPointAdjuster;
                    q.Low = (q.LowAsk + q.LowBid) / 2 * MidPointAdjuster;
                    q.Close = (q.CloseAsk + q.CloseBid) / 2 * MidPointAdjuster;

                    retVal.Add(q);
                }
            }

            return retVal;
        }

        public  OptionQuote fillQuoteFromData(DataRow row)
        {
            DateTime date = DateTime.ParseExact(row["Date"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
            date = date.AddHours(Convert.ToInt32(row["TimeBarStart"].ToString().Split(':')[0]));
            date = date.AddMinutes(Convert.ToInt32(row["TimeBarStart"].ToString().Split(':')[1]));


            OptionQuote q = new OptionQuote(date, 0, 0, 0, 0, 0);

            if (row["CallPut"].ToString() == "P")
                q.CallPut = CallPut.PUT;
            else
                q.CallPut = CallPut.CALL;

            q.JustTheDate = date.Date;

            q.Strike = Convert.ToInt32(Convert.ToDecimal(row["Strike"]));
            //q.TotalTrades = Convert.ToInt32(row["TotalTrades"].ToString());

            // int vol;
            // Int32.TryParse(row["Volume"].ToString(), out vol);


            q.Expiry = DateTime.ParseExact(row["ExpirationDate"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);


            decimal output;
            int outputInt;

            //OPEN
            // Decimal.TryParse(row["OpenBidPrice"].ToString(), out output);
            // q.OpenBid = output;

            // Int32.TryParse(row["OpenBidSize"].ToString(), out outputInt);
            // q.OpenBidVolume = outputInt;

            // Decimal.TryParse(row["OpenAskPrice"].ToString(), out output);
            // q.OpenAsk = output;

            // Int32.TryParse(row["OpenTradeSize"].ToString(), out outputInt);
            //  q.OpenAskVolume = outputInt;

            //HIGH
            // Decimal.TryParse(row["HighBidPrice"].ToString(), out output);
            // q.HighBid = output;

            // Int32.TryParse(row["HighBidSize"].ToString(), out outputInt);
            // q.HighBidVolume = outputInt;

            //  Decimal.TryParse(row["HighAskPrice"].ToString(), out output);
            // q.HighAsk = output;

            // Int32.TryParse(row["HighAskSize"].ToString(), out outputInt);
            //  q.HighAskVolume = outputInt;

            //LOW
            // Decimal.TryParse(row["LowBidPrice"].ToString(), out output);
            // q.LowBid = output;

            //  Int32.TryParse(row["LowBidSize"].ToString(), out outputInt);
            //  q.LowBidVolume = outputInt;

            // Decimal.TryParse(row["LowAskPrice"].ToString(), out output);
            // q.LowAsk = output;

            // Int32.TryParse(row["LowAskSize"].ToString(), out outputInt);
            // q.LowAskVolume = outputInt;

            //CLOSE
            Decimal.TryParse(row["CloseBidPrice"].ToString(), out output);
            q.CloseBid = output;

            Int32.TryParse(row["CloseBidSize"].ToString(), out outputInt);
            q.CloseBidVolume = outputInt;

            Decimal.TryParse(row["CloseAskPrice"].ToString(), out output);
            q.CloseAsk = output;

            Int32.TryParse(row["CloseAskSize"].ToString(), out outputInt);
            q.CloseAskVolume = outputInt;

            q.Volume = q.CloseBidVolume + q.CloseAskVolume;

            //Decimal.TryParse(row["CloseTradePrice"].ToString(), out output);
            // q.CloseTrade = output;

            // Int32.TryParse(row["CloseTradeSize"].ToString(), out outputInt);
            // q.CloseTradeVolume = outputInt;

            q.Open = (q.OpenAsk + q.OpenBid) / 2 * MidPointAdjuster;
            q.High = (q.HighAsk + q.HighBid) / 2 * MidPointAdjuster;
            q.Low = (q.LowAsk + q.LowBid) / 2 * MidPointAdjuster;

            //todo: sometimes the close or ask price can be 0, need to deal with
            q.Close = (q.CloseAsk + q.CloseBid) / 2 * MidPointAdjuster;

            return q;

        }

        public  OptionQuote fillQuoteFromDBData(DataRow row)
        {
            DateTime date = DateTime.Parse(row["dataDate"].ToString());

            OptionQuote q = new OptionQuote(date, 0, 0, 0, 0, 0);

            if (row["putCallStr"].ToString() == "PUT")
                q.CallPut = CallPut.PUT;
            else
                q.CallPut = CallPut.CALL;

            q.JustTheDate = date.Date;

            q.Strike = Convert.ToInt32(Convert.ToDecimal(row["strike"]));
         

            q.Expiry = DateTime.Parse(row["expiry"].ToString());


            decimal output;
            int outputInt;

            //CLOSE
            Decimal.TryParse(row["bid"].ToString(), out output);
            q.CloseBid = output;

            Int32.TryParse(row["bidVol"].ToString(), out outputInt);
            q.CloseBidVolume = outputInt;

            Decimal.TryParse(row["ask"].ToString(), out output);
            q.CloseAsk = output;

            Int32.TryParse(row["askVol"].ToString(), out outputInt);
            q.CloseAskVolume = outputInt;

            q.Volume = q.CloseBidVolume + q.CloseAskVolume;


            q.Open = (q.OpenAsk + q.OpenBid) / 2 * MidPointAdjuster;
            q.High = (q.HighAsk + q.HighBid) / 2 * MidPointAdjuster;
            q.Low = (q.LowAsk + q.LowBid) / 2 * MidPointAdjuster;

            //todo: sometimes the close or ask price can be 0, need to deal with
            q.Close = (q.CloseAsk + q.CloseBid) / 2 * MidPointAdjuster;

            return q;

        }


         IEnumerable<OptionQuote> GetHistoryFromFeedImport(DataRowCollection feedRows)
        {
            List<OptionQuote> retVal = new List<OptionQuote>();


            foreach (DataRow row in feedRows)
            {

                OptionQuote q = fillQuoteFromData(row);

                retVal.Add(q);

            }

            return retVal;
        }



         IEnumerable<IndexQuote> GetIndexFromFeed(DataRowCollection feedRows)
        {
            List<IndexQuote> retVal = new List<IndexQuote>();

            foreach (DataRow row in feedRows)
            {

                IndexQuote q = new IndexQuote(Convert.ToDateTime(row["date"].ToString().Replace("T00:00:00.000Z", "")), Convert.ToDecimal(row["open"]), Convert.ToDecimal(row["high"]), Convert.ToDecimal(row["low"]), Convert.ToDecimal(row["close"]), Convert.ToDecimal(row["volume"]));

                retVal.Add(q);

            }


            return retVal;
        }


        public  void removeFiles(string folder)
        {
            var dirs = Directory.GetDirectories(folder);

            foreach (var dir in dirs)
            {
                var files = Directory.GetFiles(dir + "\\RUTW", "*.csv");

                foreach (string filePath in files)
                    File.Delete(filePath);

            }

        }

        public  void setupData(string topDir, bool doExtract)
        {

            if (doExtract)
            {
                string[] zipDirs = Directory.GetDirectories(topDir);

                foreach (var dir in zipDirs)
                {
                    var newDir = Directory.GetDirectories(dir).FirstOrDefault();
                    string[] files = Directory.GetFiles(newDir, "*.csv.gz");
                    foreach (string filestr in files)
                    {
                        string filename = filestr.Replace(newDir, "").Replace("\\", "").Replace(".csv.gz", "");
                        string newFile = newDir + "\\" + filename + ".csv";

                        byte[] file = File.ReadAllBytes(filestr);
                        byte[] decompressed = Decompress(file);
                        System.IO.File.WriteAllBytes(newFile, decompressed);

                    }
                }
            }

            string[] directories = Directory.GetDirectories(topDir);


            foreach (var dir in directories)
            {
                var bottomDir = dir + @"\RUTW\";

                string[] files = Directory.GetFiles(bottomDir, "*.csv");
                string[] combined = Directory.GetFiles(bottomDir, "*_hourly.csv");

                if (combined.Length == 0)
                {

                    foreach (string filestr in files)
                    {

                        var origFile = filestr;
                        DataTable optionsData = loadData(origFile);
                        //DataTable optionsData = loadData(@"C:\Data\FOR QUANTCONNECT\CSV_Files_Combined.csv");
                        //DataTable rutIndex = loadData(@"C:\Data\FOR QUANTCONNECT\rutCloses.csv");

                        // myIndexQuotes = GetIndexFromFeed(rutIndex.Rows);
                        myQuotes = GetHistoryFromFeedImport(optionsData.Rows).OrderBy(x => x.Date);


                        myQuotes = myQuotes.OrderBy(x => x.Strike).ThenBy(x => x.CallPut).ThenBy(x => x.Date);

                        List<OptionQuote> hourly = new List<OptionQuote>();

                        int lastHour = 0;

                        //foreach(var quote in myQuotes)
                        var quoteArr = myQuotes.ToArray();
                        for (var x = 0; x < quoteArr.Length; x++)
                        {
                            var quote = quoteArr[x];

                            var bidAvg = quote.CloseBid;
                            var askAvg = quote.CloseAsk;

                            lastHour = quote.Date.Hour;

                            if (x < quoteArr.Length - 2)
                            {
                                var quote2 = quoteArr[x + 1];
                                var quote3 = quoteArr[x + 2];

                                if (quote2.Date.Hour == lastHour && quote3.Date.Hour == lastHour)
                                {
                                    bidAvg = (quote.CloseBid + quote2.CloseBid + quote3.CloseBid) / 3;
                                    askAvg = (quote.CloseAsk + quote2.CloseAsk + quote3.CloseAsk) / 3;
                                }
                            }

                            // var quote2 = myQuotes.FirstOrDefault(x => x.Strike == quote.Strike && x.Date == quote.Date.AddMinutes(1) && x.CallPut == quote.CallPut);
                            //  var quote3 = myQuotes.FirstOrDefault(x => x.Strike == quote.Strike && x.Date == quote.Date.AddMinutes(2) && x.CallPut == quote.CallPut);

                            var findIt = hourly.FirstOrDefault(x =>
                              x.Strike == quote.Strike && x.Date.Hour == lastHour && x.CallPut == quote.CallPut);

                            OptionQuote hrBar = quote;
                            hrBar.CloseBid = bidAvg;
                            hrBar.CloseAsk = askAvg;


                            if (findIt == null)
                            {
                                hourly.Add(quote);
                            }

                        }

                        using (var w = new StreamWriter(origFile.Replace(".csv", "_hourly.csv")))
                        {
                            var header = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Date", "Bid", "Ask", "Strike", "Expiry",
                              "Type", "Ask Vol", "Close Vol");
                            w.WriteLine(header);
                            w.Flush();

                            foreach (var h in hourly)
                            {
                                var line = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", h.Date, h.CloseBid, h.CloseAsk, h.Strike,
                                  h.Expiry, h.CallPut, h.CloseAskVolume, h.CloseBidVolume);
                                w.WriteLine(line);
                                w.Flush();
                            }
                        }
                    }
                }
            }
        }



         DataTable loadData(string filePath)
        {
            DataTable dt = new DataTable();

            using (StreamReader sr = new StreamReader(filePath))
            {
                string[] headers = sr.ReadLine().Split(',');


                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }

                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');//Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }


                    dt.Rows.Add(dr);
                }
            }

            return dt;

        }

         DataTable loadDataIntoList(string filePath)
        {
            DataTable dt = new DataTable();

            using (StreamReader sr = new StreamReader(filePath))
            {
                string[] headers = sr.ReadLine().Split(',');


                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }

                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');//Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }

                    dt.Rows.Add(dr);
                }
            }

            return dt;

        }


        public  void combineData(string year)
        {
            List<string> output = new List<string>();

            string sourceFolder = @"C:\Data\RUTW\opt_taq_1min\" + year + @"\";
            string destinationFile = @"C:\Data\RUTW\opt_taq_1min\" + year + @"\combined.csv";




            foreach (var week in Directory.GetDirectories(sourceFolder))
            {
                // Specify wildcard search to match CSV files that will be combined
                string[] filePaths = Directory.GetFiles(week + "\\RUTW", "*_hourly.csv");

                int i;
                for (i = 0; i < filePaths.Length; i++)
                {
                    string file = filePaths[i];

                    string[] lines = File.ReadAllLines(file);

                    lines = lines.Skip(1).ToArray(); // Skip header row for all but first file

                    foreach (string line in lines)
                    {
                        //fileDest.WriteLine(line);
                        output.Add(line);
                    }
                }

            }


            using (StreamWriter fileDest = new StreamWriter(destinationFile, true))
            {

                var header = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", "Date", "Bid", "Ask", "Strike", "Expiry",
                  "Type", "Ask Vol", "Close Vol");
                fileDest.WriteLine(header);
                fileDest.Flush();

                foreach (var line in output)
                {
                    fileDest.WriteLine(line);
                }

                fileDest.Close();

            }
        }


         byte[] Decompress(byte[] gzip)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip),
              CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }
    }
}
