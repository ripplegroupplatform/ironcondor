using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{


    public enum BuySell
    {
        BUY = 1,
        SELL = 2
    }




    public class Trade
    {
        public Trade()
        {

        }

        public Trade(decimal _price, int _amount, int _strike, DateTime _expiry, CallPut _callPut, BuySell _buySell, DateTime _tradeDate, int _volume)
        {
            tradeDate = _tradeDate;
            price = _price;
            amount = _amount;
            strike = _strike;
            expiry = _expiry;
            callPut = _callPut;
            buySell = _buySell;
            volume = _volume;
        }

        public Trade Clone()
        {
            Trade retVal = new Trade();

            retVal.tradeDate = tradeDate;
            retVal.price = price;
            retVal.amount = amount;
            retVal.strike = strike;
            retVal.expiry = expiry;
            retVal.callPut = callPut;
            retVal.buySell = buySell;
            retVal.volume = volume;

            return retVal;
        }

        public decimal price;
        public int amount;
        public int strike;
        public DateTime expiry;
        public DateTime tradeDate;
        public CallPut callPut;
        public BuySell buySell;
        public int id;
        public int volume;
    }
}
