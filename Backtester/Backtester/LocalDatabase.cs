using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
    public class LocalDatabase : IDisposable
    {
        private string mySqlConnectionString = "";

        public SqlConnection myConnectionLocal;
        public SqlCommand myCommandLocal;
        public SqlDataReader myReaderLocal = null;
        public SqlTransaction myTransLocal = null;

        public LocalDatabase()
        {
            initialize("Data Source=192.168.1.29;Initial Catalog=Backtest;User ID=toby;Password=toby; ");
        }

        public LocalDatabase(string connectionString)
        {
            initialize(connectionString);
        }

        private void initialize(string connectionString)
        {
            try
            {
                myConnectionLocal = new SqlConnection(connectionString);
                myConnectionLocal.Open();
                myCommandLocal = myConnectionLocal.CreateCommand();
                myCommandLocal.Connection = myConnectionLocal;
                myReaderLocal = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("BIG ERROR DB:" + e.Message);
            }
        }

        public long insertWithIdentity(string sql)
        {

            long retVal = 0;

            sql += " SELECT SCOPE_IDENTITY()";

            try
            {
                string sqlstring = sql;
                myCommandLocal.CommandText = sqlstring;
                retVal = Convert.ToInt64(myCommandLocal.ExecuteScalar());
            }
            catch (Exception e)
            {

            }

            return retVal;
        }


        public void insertOrUpdate(string sql)
        {
            try
            {
                string sqlstring = sql;
                myCommandLocal.CommandText = sqlstring;
                myCommandLocal.ExecuteNonQuery();
            }
            catch (Exception e)
            {

            }

        }


        public void insertOrUpdateAsync(string sql)
        {
            try
            {
                myCommandLocal.CommandText = sql;
                myCommandLocal.ExecuteNonQuery();
            }
            catch (Exception e)
            {

            }

        }

        public DataTable SelectDataTable(SqlCommand command)
        {
            DataTable dataTable = null;
            try
            {
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = command;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception e)
            {
                string debug = command.CommandText;
                Console.WriteLine(e.Message);
                // throw e;//
            }

            return dataTable;
        }

        public void Dispose()
        {
            try
            {
                myConnectionLocal.Dispose();
            }
            catch (Exception e)
            { }
        }
    }
}
