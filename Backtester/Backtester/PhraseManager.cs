﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
   

    public static class PhraseManager
    {
        public static List<string> adjectives = new List<string>();
        public static List<string> colours = new List<string>();
        public static List<string> animals = new List<string>();
        public static Random rand = new Random(DateTime.Now.ToString().GetHashCode());


        public static void loadPhraseLists()
        {
            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.myCommandLocal.CommandText = "select name from animals";

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                while (ldb.myReaderLocal.Read())
                {
                    animals.Add(ldb.myReaderLocal[0].ToString());
                }
            }

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.myCommandLocal.CommandText = "select name from adjectives";

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                while (ldb.myReaderLocal.Read())
                {
                    adjectives.Add(ldb.myReaderLocal[0].ToString());
                }
            }

            using (LocalDatabase ldb = new LocalDatabase())
            {
                ldb.myCommandLocal.CommandText = "select name from colours";

                ldb.myReaderLocal = ldb.myCommandLocal.ExecuteReader();
                while (ldb.myReaderLocal.Read())
                {
                    colours.Add(ldb.myReaderLocal[0].ToString());
                }
            }
        }


        public static string getPhrase()
        {
            int index = rand.Next(adjectives.Count);
            string adjective = adjectives[index];

            index = rand.Next(colours.Count);
            string colour = colours[index];

            index = rand.Next(animals.Count);
            string animal = animals[index];

            return adjective + " " + colour + " " + animal;
        }

    }
}
