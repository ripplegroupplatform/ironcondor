using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtester
{
  public class IronCondor
  {
    public IronCondor()
    {
      commission = 0;
    }

    public enum TradeType
    {
      IronCondor = 1,
      ShortPut_VerticalSpread = 2,
      ShortCall_VerticalSpread = 3
    }

    public DateTime tradeDate;
    public decimal commission;
    public long id;
    public decimal underlyingPriceAtStart;
    public int runID;
    public decimal underlyingPrice;
    public DateTime underlyingDate;
    public dynamic indicatorJSON;
    public TradeType tradeType = TradeType.IronCondor;
    public int contracts = 1;
    public double maxRisk = 0;
    public Settlement settlement;

    public List<Trade> parts = new List<Trade>();

        public IronCondor Clone()
        {
            IronCondor retVal = new IronCondor();

            retVal.tradeDate = this.tradeDate;
            retVal.commission = commission;
            retVal.id = id;
            retVal.underlyingPriceAtStart = underlyingPriceAtStart;
            retVal.runID = runID;
            retVal.underlyingPrice = underlyingPrice;
            retVal.underlyingDate = underlyingDate;
            retVal.indicatorJSON = indicatorJSON;
            retVal.tradeType = tradeType;
            retVal.contracts = contracts;
            retVal.maxRisk = maxRisk;

            if(settlement != null)
                retVal.settlement = settlement.Clone(retVal);

            retVal.parts = parts.ConvertAll(trade => trade.Clone());

            return retVal;
        }
  }
}
